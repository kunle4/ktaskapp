var getCreateActivity = function(who, context){
	var activity = createActivity(who,context);
	activity.what = 'created ' + context.objectType;

	return activity;
}

var getUpdateActivity = function(who, context){
	var activity = createActivity(who,context);
	activity.what = 'updated ' + context.objectType;

	return activity;
}

var getRemoveActivity = function(who, context){
	var activity = createActivity(who, context);
	activity.what = 'removed ' + context.objectType;

	return activity;
}

var getCompleteActivity = function(who, context){
	var activity = createActivity(who,context);
	activity.what = 'completed ' + context.objectType;

	return activity;
}

var createActivity = function(who, context){
	var activity = {};
	activity.who = who;
	activity.when = new Date();
	activity.context = context;

	return activity;
}

exports.getCompleteActivity = getCompleteActivity;
exports.getCreateActivity = getCreateActivity;
exports.getRemoveActivity = getRemoveActivity;
exports.getCompleteActivity = getCompleteActivity;
exports.getUpdateActivity = getUpdateActivity;