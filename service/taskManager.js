var TaskSchema = require('./../model/task');
var dbName = 'ktask';
var collectionName = 'tasks';

var mongoose = require('mongoose');
//Todo:  need to set envrionment variables to this can be dynamic
//var connection = mongoose.connect('mongodb://localhost:27017/' + dbName);
var connection = mongoose.connect('mongodb://ktask:ucandoitN0w@ds061747.mongolab.com:61747/' + dbName);
var Task = mongoose.model('Task', TaskSchema);
//mongo ds061747.mongolab.com:61747/ktask -u ktask -p ucandoitN0w

var sys = require("util");  
var test = require("assert");  

//sys.puts("Using mongodb://localhost:27017/");
sys.puts("Using mongodb://ds061747.mongolab.com:61747/");

var groupTasksByDate = function (tasks){
	var tasksByDate = [];
	for(var i=0; i< tasks.length; i++){
		var groupedTasks = [];
		if(tasksByDate[tasks[i].taskDate] != null)
			groupedTasks = tasksByDate[tasks[i].taskDate];
		groupedTasks.push(tasks[i]);
		tasksByDate[tasks[i].taskDate] = groupedTasks;	
	}
	return tasksByDate;
};

exports.groupTasksByDate = groupTasksByDate;