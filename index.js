var http = require('http');
var express = require('express');
var app = express();
var mongoStore = require('connect-mongodb');

var staticServer = require('./controller/static');
var taskController = require('./controller/task');
var projectController = require('./controller/project');
var userController = require('./controller/user');

/** user kunle4@gmail.com password is password, id: 4f4303bf5173f1f416000001 **/
var port = process.env.PORT || 3000;
var sys = require("util");  
var test = require("assert"); 

var dbName = 'ktask';

app.configure(function(){ 
    
	app.use(express.cookieParser());
	app.use(express.session({ secret: "keyboard cat" }));
	app.use(express.session({ store: mongoStore(app.set('sessionStore')), secret: 'topsecret' }));		    
	app.use(express.methodOverride()); 
	app.use(express.bodyParser());
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
	app.use(app.router);
	app.use(
			function(req, res, next){
				req.locals.user = req.session.user;
				req.locals.messages = req.session.messages;
				next();
			}
		);		
});
app.set('views', __dirname + '/view');
app.set('view engine', 'jade'); //jade setup
app.set('view options', { pretty: true });


/*** helpers ****/
function requiresLogin(req, res, next) {
  if (req.session.user) {
    next();
  } else {
    //res.redirect('/login/new?redir=' + req.url);
    res.redirect('/login');
  }
};


/****** routers *****************/
app.get('/logout', function(req, res){
	console.log('Killing session and logging out for ' + req.session.user.username);
	req.session.user = null;
	res.redirect('/');
});
app.get('/login', function(req, res) {
  	req.url = '/view/login/index.html';
	staticServer.getFile(req, res);
});

app.get('/angular', function(req, res) {
    //if the user is logged in then redirect to project page for angular
    if(req.session.user && req.session.user.username)
        req.url = '/view/angular/app/index.html';
    else
        req.url = '/view/angular/app/pre_login.html';
    staticServer.getFile(req, res);
});
app.get('/login/messages', function(req, res) {
  res.end(JSON.stringify(req.session.messages));
});
app.get('/login/*', function(req, res) {
  res.redirect('/login');
});
/*app.get('/login/new', function(req, res) {
  res.render('login/new', {locals: {
    redir: '/'//req.query.redir
  }});
});*/

app.get('/add/user', function(req, res) {
	req.body.firstname = 'kunle';
	req.body.lastname = 'adeniyi';
	req.body.password = 'password';
	req.body.username = 'kunle';
	userController.addUser(req, res);
});

app.post('/login', function(req, res) {
	userController.authenticate(req.body.login, req.body.password,function(user){
    if (user != null && typeof(user) != undefined) {
      console.log('User logged in '+ user.username);
      console.log('going to redirect to :', req.body.redir);
      req.session.user = user;
      res.redirect(req.body.redir || '/backbone');
    } 
    else {
    	console.log('This user is not defined, login failed');
		req.session.messages=['Login failed','Username or password not known, please try again!'];
        //console.log(req);
		//res.redirect('/login/new?redir=' + req.url);
		res.redirect(req.body.redir || '/login');
    }
  });
});

app.get('/taskshtml',requiresLogin, function(req, res){
	console.log("Routing to task controller: "+ req.url);
	taskController.getTasksHtml(req, res );	
});  

app.get('/task/bytitle/:title',function(req,res){
	console.log("Routing to task controller: "+ req.url);
	taskController.getTasksByTitle(req.params.title, req,res);   
});

app.get('/view/*',function(req, res){
	staticServer.getFile(req, res);
});

app.get('/favicon.ico',function(req, res){
	return;
});

app.post('/add/task', function(req, res){
	console.log("Routing to task controller: "+ req.url);
	console.log("Adding task for " + req.session.user.username);
	var tDate = new Date(req.body.taskDate);
	var newTask = {taskDate:tDate,title:req.body.title, description:req.body.description, note:req.body.note,owner:req.session.user._id, tags: req.body.tags.split(",")};
	taskController.addTask(res,newTask);
});

app.post('/backbone/add/task', function(req, res){
	console.log("Routing to task controller: "+ req.url);
	console.log("Adding task for " + req.session.user.username);
	var newTask = req.body;
	console.log(newTask);
	newTask.owner = req.session.user._id
	taskController.addTask(res,newTask);
});

app.post('/update/task/:id', function(req, res){
    console.log("Routing to task controller: "+ req.url);
    console.log("updating task for " + req.session.user.username);
    var tDate = new Date(req.body.taskDate);
    var task = {id:req.params.id, taskDate:tDate,title:req.body.title, description:req.body.description, note:req.body.note,owner:req.session.user._id, tags: req.body.tags.split(",")};
    taskController.updateTask(res,task);
});

app.post('/backbone/update/task/:id', function(req, res){
    console.log("Routing to task controller: "+ req.url);
    var task = req.body;
	console.log(task);
	task.owner = req.session.user._id
	taskController.updateTask(res,task);
});
/****************** PROJECT REST API****************************/
app.get('/projects/:id/tasks/statuses',requiresLogin,function(req, res){
    console.log("Routing to project controller: "+ req.url);
    console.log(req.query);

    projectController.getStatuses(res,req.params.id);
});

app.get('/projects/:id/tasks/', function(req, res){
    var projectId = req.params.id;
    console.log('getting the tasks for: ', projectId);
    var filter = req.query;

    projectController.getTasks(res,projectId,filter);
});

app.get('/projects/:id/tasks', function(req, res){
    var projectId = req.params.id;
    console.log('getting the tasks for: ', projectId);
    var filter = req.query;

    projectController.getTasks(res,projectId,filter);
});

app.get('/projects/',requiresLogin,function(req, res){
	console.log("Routing to project controller: "+ req.url);
	console.log(req.query);
	projectController.getProjects(req, res);
});

app.delete('/projects/:id', requiresLogin,function(req, res){
    console.log("Routing to project controller: "+ req.url);
    projectController.removeProject(res,req.params.id);
});

app.get('/projects/getlowestpriority/:id', function(req, res){
    var projectId = req.params.id;
    console.log('getting the lowest priority for project: ', projectId);
    projectController.getLowestProrityTask(projectId,function(err,lTasks){
        if(err)
            console.log('error: ', err);
        res.write(JSON.stringify(lTasks));
        res.end();
    });
});

app.get('/projects',requiresLogin,function(req, res){
	console.log("Routing to project controller: "+ req.url);
	console.log(req.query);
	projectController.getProjects(req, res);
});

app.post('/projects/:id/tasks/complete', function(req, res){
    console.log("Routing to project controller: "+ req.url);
    projectController.completeTask(res,req.params.id, req.body);
});

//updating tasks
app.put('/projects/:id/tasks/:taskId', requiresLogin,function(req, res){
    console.log("Routing to project controller: "+ req.url);
    var task = req.body;
    console.log(task);
    task.owner = req.session.user._id
    projectController.updateTask(res,task,req.params.id);
});

//Adding new task
app.post('/projects/:id/tasks/', requiresLogin,function(req, res){
    console.log("Routing to project controller: "+ req.url);
    console.log("Routing to project controller: "+ req.params.id);
    var task = req.body;
    console.log(task);
    task.owner = req.session.user._id
    projectController.addTask(res,task,req.params.id);
});

app.post('/projects/:id/tasks', requiresLogin,function(req, res){
    console.log("Routing to project controller: "+ req.url);
    console.log("Routing to project controller: "+ req.params.id);
    var task = req.body;
    console.log(task);
    task.owner = req.session.user._id
    projectController.addTask(res,task,req.params.id);
});

app.delete('/projects/:id/tasks/:taskId', requiresLogin,function(req, res){
    console.log("Routing to project controller: "+ req.url);
    projectController.removeTask(res,req.params.taskId,req.params.id);
});

app.post('/projects/', requiresLogin, function(req, res){
	console.log("Routing to project controller: "+ req.url);
	console.log("Adding project for " + req.session.user.username);
	var newProject = req.body;
	if(typeof newProject._id != "undefined")
		delete newProject._id;
	console.log(newProject);
	newProject.owner = req.session.user._id
	projectController.addProject(res,newProject);
});

app.post('/projects', requiresLogin, function(req, res){
    console.log("Routing to project controller: "+ req.url);
    console.log("Adding project for " + req.session.user.username);
    var newProject = req.body;
    if(typeof newProject._id != "undefined")
        delete newProject._id;
    console.log(newProject);
    newProject.owner = req.session.user._id
    projectController.addProject(res,newProject);
});

app.put('/projects/:id', function(req, res){
    console.log("Routing to project controller: "+ req.url);
    var project = req.body;
	console.log(project);
	project.owner = req.session.user._id
	projectController.updateProject(res,project);
});

app.get('/projects/:id/count_per_group',requiresLogin,function(req, res){
    console.log("Routing to project controller: "+ req.url);
    console.log(req.query);

    projectController.getGroupFacets(res,req.params.id, req.query);
});

app.get('/projects/count_per_project',requiresLogin,function(req, res){
    console.log("Routing to project controller: "+ req.url);

    projectController.getTaskCountPerProject(res,req.session.user._id);
});

app.post('/projects/:id/prioritize/', function(req, res){
    console.log("Routing to project controller: "+ req.url);
    projectController.updateTasksPriority(res,req.params.id, req.body);
});

/****************** TASKS REST API***************************/

app.get('/tasks/',requiresLogin,function(req, res){
	console.log("Routing to task controller: "+ req.url);
	taskController.getTasks(req, res);
});

app.get('/tasks',requiresLogin,function(req, res){
	console.log("Routing to task controller: "+ req.url);
	taskController.getTasks(req, res);
});

app.get('/tasks/allstatuses',function(req,res){
	console.log("Routing to task controller: "+ req.url);
	console.log("query string =  "+ req.query);
	taskController.getAllStatuses(res,req);
});

app.get('/tasks/statuses',function(req,res){
	console.log("Routing to task controller: "+ req.url);
	console.log("query string =  "+ req.query);
	taskController.getStatuses(res,req);
});

app.get('/tasks/count_per_project',requiresLogin,function(req, res){
	console.log("Routing to tasks controller: "+ req.url);
	console.log(req.query);
	taskController.getTaskCountPerProject(res,req.session.user._id);
});

app.get('/tasks/count_per_group',requiresLogin,function(req, res){
	console.log("Routing to tasks controller: "+ req.url);
	console.log(req.query);

	taskController.getGroupFacets(res,req);
});

app.post('/tasks/', function(req, res){
	console.log("Routing to task controller: "+ req.url);
	console.log("Adding task for " + req.session.user.username);
	var newTask = req.body;
	if(typeof newTask._id != "undefined")
		delete newTask._id;
	console.log(newTask);
	newTask.owner = req.session.user._id
	taskController.addTask(res,newTask);
});

app.post('/tasks/prioritize', function(req, res){
	console.log("Routing to task controller: "+ req.url);
	//console.log("Array of priorities " + req.body);
	taskController.updateTasksPriority(res, req.body);
});

app.post('/tasks/complete', function(req, res){
	console.log("Routing to task controller: "+ req.url);
	//console.log("Array of priorities " + req.body);
	taskController.completeTask(res, req.body);
});

app.post('/tasks/update_batch', function(req, res){
	console.log("Routing to task controller: "+ req.url);
	console.log("going to update: ");
	console.log(req.body);

	taskController.updateTasks(res, req.body);
});

app.put('/tasks/:id', function(req, res){
    console.log("Routing to task controller: "+ req.url);
    var task = req.body;
	console.log(task);
	task.owner = req.session.user._id
	taskController.updateTask(res,task);
});

app.delete('/tasks/:id', function(req, res){
	console.log("Routing to task controller: "+ req.url);
	console.log("the request param" + req.params.id);
	taskController.removeTask(res,req.params.id);
});

/*************** User routers ****************************/

app.get('/user/byemail/:email',function(req, res){
	console.log("Routing to user controller: "+ req.url);
	userController.getUserByEmail(req.params.email, req,res); 
});

app.get('/user/current',function(req, res){
	console.log("Returning current logged in user: "+ req.url);
	res.write(JSON.stringify(req.session.user));
   	res.end();	
});

app.get('/user/:id',function(req, res){
	console.log("Routing to user controller: "+ req.url);
	userController.getUserById(req.params.id, req,res); 
});

app.post('/register', function(req, res){
	console.log("Routing to user controller: "+ req.url);
	//userController.registerUser(req,res);
	userController.registerUser(req,res,function(user){
		    if (user != null && typeof(user) != undefined) {
		      console.log('User logged in '+ user.username);
		      req.session.user = user;
		      res.redirect(req.body.redir || '/backbone');
		    }
    	}
    ); 
});

/*********************************************************/
app.get('/backbone',requiresLogin, function(req, res){
	req.url = '/view/backbone/index.html';
	staticServer.getFile(req, res);
});

app.get('/*',requiresLogin, function(req, res){
	//req.url = '/view/index.html';
	req.url = '/view/backbone/index.html';
	staticServer.getFile(req, res);
});
app.post('/messages/clear', function(req, res){
	console.log("clearing messages: "+ req.url);
	req.session.messages = null;
	res.end(JSON.stringify({done:true}));
});

app.post('/*', function(req, res){
	staticServer.getFile(req, res);
});

app.listen(port);
console.log('Server running using express on http://127.0.0.1:'+port+'/');