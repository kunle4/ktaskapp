
/** tasks indexes **/
db.getCollection("tasks").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tasks indexes **/
db.getCollection("tasks").ensureIndex({
  "title": NumberInt(1)
},{
  "unique": false
});

/** tasks indexes **/
db.getCollection("tasks").ensureIndex({
  "taskDate": NumberInt(1)
},{
  "unique": false
});

/** tasks indexes **/
db.getCollection("tasks").ensureIndex({
  "activities.when": NumberInt(1)
},[
  
]);

/** tasks records **/
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50a81d907f2b453f22000014"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Some sort of animation using our beloved jquery",
  "group": "UI\/UX",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(17),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-11-17T20:31:27.437Z"),
  "tasks": [
    {
      "todo": "slide right for registration",
      "_id": ObjectId("50a81d907f2b453f22000016"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "slide left for login",
      "_id": ObjectId("50a81d907f2b453f22000015"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Animation on login and registration "
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50de990f3ca0f4eb25000002"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(6),
  "project": ObjectId("51041ad75467feb313000009"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2012-12-29T07:16:18.895Z"),
  "tasks": [
    {
      "todo": "blah2",
      "_id": ObjectId("50de990f3ca0f4eb25000003"),
      "complete": true
    },
    {
      "todo": "blah ",
      "_id": ObjectId("50de990f3ca0f4eb25000004"),
      "complete": true
    }
  ],
  "title": "for testing "
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50a81d397f2b453f22000010"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Display tasks on a calendar",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(5),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2012-11-17T20:31:27.437Z"),
  "tasks": [
    {
      "todo": "Find calendar lib",
      "_id": ObjectId("50a81d397f2b453f22000013"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "look at jquery.calendar ",
      "_id": ObjectId("50a81d397f2b453f22000012"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "integrate with backbone",
      "_id": ObjectId("50a81d397f2b453f22000011"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Calendar View"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50b0f95983629c8323000009"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T23:48:01.569Z"),
  "description": "Add a namespace to avoid conflicts and have the app be encapsulated within one main app class",
  "group": "Front End",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(1),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2012-11-18T06:04:08.272Z"),
  "tasks": [
    {
      "todo": "hook up app with new task click",
      "_id": ObjectId("50b0f95983629c832300000a"),
      "completionDate": ISODate("2013-02-19T03:52:05.432Z"),
      "complete": true
    },
    {
      "todo": "add every view and model to app",
      "_id": ObjectId("50b0f95983629c832300000b"),
      "completionDate": ISODate("2013-02-19T03:52:05.432Z"),
      "complete": true
    },
    {
      "todo": "create central app class",
      "_id": ObjectId("50b0f95983629c832300000c"),
      "completionDate": ISODate("2013-02-19T03:52:05.432Z"),
      "complete": true
    }
  ],
  "title": "Central global app variable"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50d281254d5206150d00000d"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T23:26:07.816Z"),
  "description": "Parting to the break of dawn",
  "group": "A",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(2),
  "project": ObjectId("51041ad75467feb313000009"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-16T18:54:43.103Z"),
  "tasks": [
    {
      "todo": "entertainment",
      "_id": ObjectId("50d281254d5206150d00000e"),
      "completionDate": ISODate("2013-02-23T17:51:49.961Z"),
      "complete": true
    },
    {
      "todo": "people",
      "_id": ObjectId("50d281254d5206150d00000f"),
      "completionDate": ISODate("2013-02-23T17:51:49.961Z"),
      "complete": true
    },
    {
      "todo": "music",
      "_id": ObjectId("50d281254d5206150d000010"),
      "completionDate": ISODate("2013-02-23T17:51:49.961Z"),
      "complete": true
    },
    {
      "todo": "alcohol",
      "_id": ObjectId("50d281254d5206150d000012"),
      "completionDate": ISODate("2013-02-23T17:51:49.961Z"),
      "complete": true
    },
    {
      "todo": "food",
      "_id": ObjectId("50d281254d5206150d000011"),
      "completionDate": ISODate("2013-02-23T17:51:49.961Z"),
      "complete": true
    }
  ],
  "title": "Celebration "
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50d3cef309b7db641100000a"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T22:47:50.480Z"),
  "description": "Make tasks restful",
  "group": "API",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(15),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-21T01:31:28.975Z"),
  "tasks": [
    {
      "todo": "Tasks GET",
      "_id": ObjectId("50d3cef309b7db641100000b"),
      "completionDate": ISODate("2013-02-19T03:52:31.330Z"),
      "complete": true
    },
    {
      "todo": "Tasks PUT",
      "_id": ObjectId("50d3cef309b7db641100000e"),
      "completionDate": ISODate("2013-02-19T03:52:31.330Z"),
      "complete": true
    },
    {
      "todo": "Tasks POST",
      "_id": ObjectId("50d3cef309b7db641100000d"),
      "completionDate": ISODate("2013-02-19T03:52:31.330Z"),
      "complete": true
    },
    {
      "todo": "Tasks DELETE",
      "_id": ObjectId("50d3cef309b7db641100000c"),
      "completionDate": ISODate("2013-02-19T03:52:31.330Z"),
      "complete": true
    }
  ],
  "title": "Proper Tasks RESTful API"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50ddb520e7a4797d23000008"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Updating all views across the network without the user refreshing",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(14),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-28T15:02:32.763Z"),
  "tasks": [
    {
      "todo": "ajax refresh?",
      "_id": ObjectId("50ddb520e7a4797d2300000a"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "web sockets",
      "_id": ObjectId("50ddb520e7a4797d2300000b"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "socket.io",
      "_id": ObjectId("50ddb520e7a4797d2300000c"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "push\/pub",
      "_id": ObjectId("50ddb520e7a4797d23000009"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Real time updates (Sockets)"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50ddb5ebe7a4797d2300000d"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Expanding and extending the app. More around user management\n http:\/\/web.appstorm.net\/roundups\/task-management\/top-10-apps-web-based-task-managers\/",
  "group": "Idea",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(10),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    "idea",
    "concept"
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-28T15:05:44.841Z"),
  "tasks": [
    {
      "todo": "user management",
      "_id": ObjectId("50ddb5ebe7a4797d23000010"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "invites",
      "_id": ObjectId("50ddb5ebe7a4797d2300000e"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "collaboration",
      "_id": ObjectId("50ddb5ebe7a4797d2300000f"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "BIG Ideas "
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50de5172e7a4797d2300001c"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T22:47:55.596Z"),
  "description": "Steps to do % completion, lets do it on the client",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(17),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-28T23:23:39.416Z"),
  "tasks": [
    {
      "todo": "handle case where no todos",
      "_id": ObjectId("50de5172e7a4797d2300001d"),
      "completionDate": ISODate("2013-02-19T03:53:06.921Z"),
      "complete": true
    },
    {
      "todo": "calculation = total complete\/total todos",
      "_id": ObjectId("50de5172e7a4797d23000020"),
      "completionDate": ISODate("2013-02-19T03:53:06.921Z"),
      "complete": true
    }
  ],
  "title": "percentage completion"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50de993e3ca0f4eb25000005"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(4),
  "project": ObjectId("51041ac75467feb313000007"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-29T07:17:52.644Z"),
  "tasks": [
    {
      "todo": "ontario",
      "_id": ObjectId("50de993e3ca0f4eb25000006"),
      "complete": true
    },
    {
      "todo": "sydney",
      "_id": ObjectId("50de993e3ca0f4eb25000007"),
      "complete": true
    }
  ],
  "title": "another test"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50d920b31ad3f5c219000002"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": null,
  "description": "drink drink drink",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(7),
  "project": ObjectId("51041ad75467feb313000009"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-25T03:41:30.172Z"),
  "tasks": [
    {
      "todo": "whisky!!!!!!!!!!!!!!!!!!!!!!!!!!",
      "_id": ObjectId("50d920b31ad3f5c219000004"),
      "complete": true
    },
    {
      "todo": "gin",
      "_id": ObjectId("50d920b31ad3f5c219000007"),
      "complete": true
    },
    {
      "todo": "Irish cream",
      "_id": ObjectId("50d920b31ad3f5c219000003"),
      "complete": true
    },
    {
      "todo": "vodka",
      "_id": ObjectId("50d920b31ad3f5c219000006"),
      "complete": true
    },
    {
      "todo": "rum",
      "_id": ObjectId("50d920b31ad3f5c219000005"),
      "complete": true
    }
  ],
  "title": "Alcohol"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50df28b00f8c752727000002"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(2),
  "project": ObjectId("51041a955467feb313000005"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2012-12-29T17:27:36.524Z"),
  "tasks": [
    {
      "todo": "party starter",
      "_id": ObjectId("50df28b00f8c752727000005"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "decor",
      "_id": ObjectId("50df28b00f8c752727000006"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "theme",
      "_id": ObjectId("50df28b00f8c752727000007"),
      "completionDate": ISODate("2013-01-26T21:23:14.139Z"),
      "complete": true
    },
    {
      "todo": "people",
      "_id": ObjectId("50df28b00f8c752727000003"),
      "completionDate": ISODate("2013-01-26T21:23:14.139Z"),
      "complete": true
    },
    {
      "todo": "music",
      "_id": ObjectId("50df28b00f8c752727000004"),
      "completionDate": ISODate("2013-01-26T21:23:14.139Z"),
      "complete": true
    }
  ],
  "title": "2013 party all night"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e1f9430f8c75272700000f"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-26T21:27:27.569Z"),
  "description": "Similar to project contains a group of similar tasks defined by the user",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(1),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2012-12-30T19:53:25.840Z"),
  "tasks": [
    {
      "todo": "Controller methods",
      "_id": ObjectId("50e1f9430f8c752727000014"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    },
    {
      "todo": "drag and drop into groups",
      "_id": ObjectId("50e1f9430f8c752727000010"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    },
    {
      "todo": "router method",
      "_id": ObjectId("50e1f9430f8c752727000013"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    },
    {
      "todo": "Model instance and class methods",
      "_id": ObjectId("50e1f9430f8c752727000015"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    },
    {
      "todo": "view template for groups area",
      "_id": ObjectId("50e1f9430f8c752727000011"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    },
    {
      "todo": "front end model",
      "_id": ObjectId("50e1f9430f8c752727000012"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    },
    {
      "todo": "group controller",
      "_id": ObjectId("50eb92ff0e0db3500d000002"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    },
    {
      "todo": "mapReduce for groups (Faucet like)",
      "_id": ObjectId("50e1f9430f8c752727000016"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    },
    {
      "todo": "default group for task?",
      "_id": ObjectId("50e1f9430f8c752727000017"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    },
    {
      "todo": "Backend model update",
      "_id": ObjectId("50e1f9430f8c752727000018"),
      "completionDate": ISODate("2013-02-19T03:51:26.65Z"),
      "complete": true
    }
  ],
  "title": "Custom grouping of tasks"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e1fa070f8c752727000019"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Progress and performance reports and also dashboards",
  "group": "Analytics",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(16),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2012-12-30T19:53:25.840Z"),
  "tasks": [
    {
      "todo": "research charting libraries",
      "_id": ObjectId("50e1fa070f8c75272700001d"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "research grid libraries",
      "_id": ObjectId("50e1fa070f8c75272700001c"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "More map reduce",
      "_id": ObjectId("50e1fa070f8c75272700001b"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "backbone integration with other lib",
      "_id": ObjectId("50e1fa070f8c75272700001a"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Dashboard: Reports and charts"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e202fd0f8c75272700001e"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Toolbar that contains instance buttons for specific functions like copy\/clone, move to, complete, archive ....\nToolbar for projects and tasks",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(12),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2012-12-30T19:53:25.840Z"),
  "tasks": [
    {
      "todo": "storing of context on select",
      "_id": ObjectId("50e202fd0f8c752727000025"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "highlighting the selected instance",
      "_id": ObjectId("50e202fd0f8c752727000024"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "using arrow keys to navigate?",
      "_id": ObjectId("50e202fd0f8c752727000023"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "toolbar design",
      "_id": ObjectId("50e202fd0f8c752727000022"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "icons",
      "_id": ObjectId("50e202fd0f8c752727000020"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "wiring with backbone",
      "_id": ObjectId("50e202fd0f8c75272700001f"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Context toolbar"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e205210f8c752727000027"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Launch to a cloud. Cloud foundry, rackspace, amazon, joyent, engine yard\nhttps:\/\/github.com\/punkave\/stagecoach\nhttp:\/\/servergrove.com\/vps\nhttp:\/\/www.rackspace.com\/cloud\/servers\/pricing\/\nhttp:\/\/www.linode.com\/",
  "group": "Deploying",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(11),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2012-12-30T19:53:25.840Z"),
  "tasks": [
    {
      "todo": "research clouds",
      "_id": ObjectId("50e205210f8c75272700002a"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "choose a cloud",
      "_id": ObjectId("50e205210f8c752727000029"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "deploy to the cloud",
      "_id": ObjectId("50e205210f8c752727000028"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Cloud Launch"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e224a4331b95f328000002"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(4),
  "project": ObjectId("51041ac75467feb313000007"),
  "status": "ongoing",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-31T23:49:26.155Z"),
  "tasks": [
    {
      "todo": "3",
      "_id": ObjectId("50e224a4331b95f328000003"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "2",
      "_id": ObjectId("50e224a4331b95f328000004"),
      "completionDate": ISODate("2013-01-06T23:41:21.340Z"),
      "complete": true
    },
    {
      "todo": "1",
      "_id": ObjectId("50e224a4331b95f328000005"),
      "completionDate": ISODate("2013-01-06T23:41:21.340Z"),
      "complete": true
    }
  ],
  "title": "new task that will get completed fast"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e3acef7dc7db472b000011"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(3),
  "project": ObjectId("51041a955467feb313000005"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-02T03:43:29.130Z"),
  "tasks": [
    {
      "todo": "more",
      "_id": ObjectId("50e3ad177dc7db472b000016"),
      "complete": true
    },
    {
      "todo": "added",
      "_id": ObjectId("50e3ad0d7dc7db472b000015"),
      "complete": true
    },
    {
      "todo": "1",
      "_id": ObjectId("50e3acef7dc7db472b000012"),
      "complete": true
    }
  ],
  "title": "fbi"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e3b52320e5ba6b08000004"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(5),
  "project": ObjectId("51041ac75467feb313000007"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-02T04:18:14.167Z"),
  "tasks": [
    {
      "todo": "1",
      "_id": ObjectId("50e3b52320e5ba6b08000005"),
      "complete": true
    }
  ],
  "title": "backbone again"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e654b1510641b708000004"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T23:45:50.360Z"),
  "description": "",
  "group": "K",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(1),
  "project": ObjectId("51041ad75467feb313000009"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-04T04:03:20.546Z"),
  "tasks": [
    {
      "todo": "how many",
      "_id": ObjectId("50e654b1510641b708000005"),
      "completionDate": ISODate("2013-02-23T17:51:59.987Z"),
      "complete": true
    }
  ],
  "title": "kids"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50d28cf04d5206150d000013"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T22:47:39.794Z"),
  "description": "Main task completion and sub task completion",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(2),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-16T18:54:43.103Z"),
  "tasks": [
    {
      "todo": "Accommodate for completion date",
      "_id": ObjectId("50d3dfcd8c3f9a3612000008"),
      "completionDate": ISODate("2013-02-19T03:52:21.435Z"),
      "complete": true
    },
    {
      "todo": "when complete de-prioritize?",
      "_id": ObjectId("50dd34f2e7a4797d23000007"),
      "completionDate": ISODate("2013-02-19T03:52:21.435Z"),
      "complete": true
    },
    {
      "todo": "Find icons",
      "_id": ObjectId("50d28cf04d5206150d000015"),
      "completionDate": ISODate("2013-02-19T03:52:21.435Z"),
      "complete": true
    },
    {
      "todo": "main task completion",
      "_id": ObjectId("50d28cf04d5206150d000016"),
      "completionDate": ISODate("2013-02-19T03:52:21.435Z"),
      "complete": true
    },
    {
      "todo": "subtask completion",
      "_id": ObjectId("50d28cf04d5206150d000017"),
      "completionDate": ISODate("2013-02-19T03:52:21.435Z"),
      "complete": true
    },
    {
      "todo": "use css magic where possible",
      "_id": ObjectId("50d28cf04d5206150d000014"),
      "completionDate": ISODate("2013-02-19T03:52:21.435Z"),
      "complete": true
    }
  ],
  "title": "Task Completion"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50dd143c39c27d0722000002"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T22:47:53.173Z"),
  "description": "Make new task button always appear",
  "group": "UI\/UX",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(16),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-28T03:35:45.740Z"),
  "tasks": [
    {
      "todo": "put task button on the left",
      "_id": ObjectId("50de29e6e7a4797d2300001b"),
      "completionDate": ISODate("2013-02-19T03:52:53.521Z"),
      "complete": true
    },
    {
      "todo": "investigate putting new task button on the left side",
      "_id": ObjectId("50dd143c39c27d0722000003"),
      "completionDate": ISODate("2013-02-19T03:52:53.521Z"),
      "complete": true
    }
  ],
  "title": "New Task button visibility"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50ba7de65d8fce6b08000002"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T22:47:58.296Z"),
  "description": "Tasks updates from ui to server persists.",
  "group": "UI\/UX",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(18),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-01T21:15:08.253Z"),
  "tasks": [
    {
      "todo": "persist saves to db",
      "_id": ObjectId("50ba7de65d8fce6b08000004"),
      "completionDate": ISODate("2013-02-19T03:53:21.618Z"),
      "complete": true
    },
    {
      "todo": "update UI appropriately",
      "_id": ObjectId("50d3ae016dabfcd40f00000a"),
      "completionDate": ISODate("2013-02-19T03:53:21.618Z"),
      "complete": true
    },
    {
      "todo": "make it intuitive?",
      "_id": ObjectId("50d3ae016dabfcd40f000009"),
      "completionDate": ISODate("2013-02-19T03:53:21.618Z"),
      "complete": true
    }
  ],
  "title": "Fix updating tasks UI"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50d8a02f73be325515000018"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T22:49:33.369Z"),
  "description": "Sort order for tasks to facilitate prioritizing and drag&drop for UI. Masonry does not work well with sortable",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(2),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-24T18:25:58.628Z"),
  "tasks": [
    {
      "todo": "move completed tasks to a different bucket",
      "_id": ObjectId("50ddb65de7a4797d23000011"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "percentage completion",
      "_id": ObjectId("50ddb802e7a4797d23000018"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "move completed subtask to top of completed pile",
      "_id": ObjectId("50de0b96e7a4797d23000019"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "ability to prioritize subtasks on new task",
      "_id": ObjectId("50ddb771e7a4797d23000017"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "remove masonry plugin",
      "_id": ObjectId("50dca2f751a2a1a21f000002"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "Sort order on task model",
      "_id": ObjectId("50d8a02f73be32551500001e"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "Sort order on backbone model",
      "_id": ObjectId("50d8a02f73be32551500001d"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "prioritizing subtasks",
      "_id": ObjectId("50dcd921cef71be220000002"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "Drag and drop lib",
      "_id": ObjectId("50d8a02f73be32551500001a"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "Process for re-prioritizing",
      "_id": ObjectId("50d8a02f73be32551500001c"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "Controller update",
      "_id": ObjectId("50d8a02f73be32551500001b"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    },
    {
      "todo": "update backbone views",
      "_id": ObjectId("50d8a02f73be325515000019"),
      "completionDate": ISODate("2013-02-19T03:52:15.332Z"),
      "complete": true
    }
  ],
  "title": "Prioritizing Tasks"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e3aa017dc7db472b00000d"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T23:47:08.977Z"),
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(1),
  "project": ObjectId("51041ac75467feb313000007"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-02T03:31:00.414Z"),
  "tasks": [
    {
      "todo": "1",
      "_id": ObjectId("50e3aa017dc7db472b00000e"),
      "completionDate": ISODate("2013-01-06T23:47:08.977Z"),
      "complete": true
    }
  ],
  "title": "lets complete a few"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50f19d99e2ceafd50b000004"),
  "activities": [
    
  ],
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(5),
  "project": ObjectId("51041ac75467feb313000007"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-12T17:29:42.926Z"),
  "tasks": [
    {
      "todo": "2",
      "_id": ObjectId("50f19d99e2ceafd50b000005"),
      "complete": false
    }
  ],
  "title": "2"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50f19da1e2ceafd50b000006"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "This task is not done",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(1),
  "project": ObjectId("51041a955467feb313000005"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-12T17:29:42.926Z"),
  "tasks": [
    {
      "todo": "3",
      "_id": ObjectId("50f19da1e2ceafd50b000007"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "yo not done",
      "completionDate": null,
      "_id": ObjectId("50f19dfce2ceafd50b00000e"),
      "complete": false
    }
  ],
  "title": "3"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50f19da9e2ceafd50b000008"),
  "activities": [
    
  ],
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(6),
  "project": ObjectId("51041ad75467feb313000009"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-12T17:29:42.926Z"),
  "tasks": [
    {
      "todo": "4",
      "_id": ObjectId("50f19da9e2ceafd50b000009"),
      "complete": false
    }
  ],
  "title": "4"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50f19db2e2ceafd50b00000a"),
  "activities": [
    
  ],
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(7),
  "project": ObjectId("51041ad75467feb313000009"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-12T17:29:42.926Z"),
  "tasks": [
    {
      "todo": "5",
      "_id": ObjectId("50f19db2e2ceafd50b00000b"),
      "complete": false
    }
  ],
  "title": "5"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50f19db9e2ceafd50b00000c"),
  "activities": [
    
  ],
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(2),
  "project": ObjectId("51041ac75467feb313000007"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-12T17:29:42.926Z"),
  "tasks": [
    {
      "todo": "6",
      "_id": ObjectId("50f19db9e2ceafd50b00000d"),
      "complete": false
    }
  ],
  "title": "6"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e3a4947dc7db472b000003"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(8),
  "project": ObjectId("51041ad75467feb313000009"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-02T03:07:52.430Z"),
  "tasks": [
    {
      "todo": "1",
      "_id": ObjectId("50e3a4947dc7db472b000004"),
      "complete": true
    }
  ],
  "title": "New task"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e3a5b37dc7db472b000009"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(1),
  "project": ObjectId("51041a955467feb313000005"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-02T03:12:18.813Z"),
  "tasks": [
    {
      "todo": "happy",
      "_id": ObjectId("50e3a5b37dc7db472b00000a"),
      "complete": true
    }
  ],
  "title": "good people"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50e3a96e7dc7db472b00000b"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": null,
  "description": "",
  "owner": ObjectId("509ebe438b358df30b000001"),
  "priority": NumberInt(2),
  "project": ObjectId("51041a955467feb313000005"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-01-02T03:17:02.461Z"),
  "tasks": [
    {
      "todo": "1",
      "_id": ObjectId("50e3a96e7dc7db472b00000c"),
      "complete": true
    }
  ],
  "title": "new "
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50ddb718e7a4797d23000012"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Having buckets. Completed,  ongoing, and archiving or parked based on status\nMore groping on tasks",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(3),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    "grouping",
    "organization"
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-28T15:05:44.841Z"),
  "tasks": [
    {
      "todo": "drag and drop to ongoing",
      "completionDate": null,
      "_id": ObjectId("512ed7b01f110f6d08000004"),
      "complete": false
    },
    {
      "todo": "drag and drop to archive",
      "_id": ObjectId("50ddb718e7a4797d23000013"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "api for completion of a task",
      "_id": ObjectId("510db597e0b944ea0d00000b"),
      "completionDate": ISODate("2013-02-28T04:06:08.18Z"),
      "complete": true
    },
    {
      "todo": "drag and drop to completion bucket",
      "_id": ObjectId("50ddb718e7a4797d23000014"),
      "completionDate": ISODate("2013-02-28T04:06:08.18Z"),
      "complete": true
    },
    {
      "todo": "groping within projects",
      "_id": ObjectId("51125d84c8f2f1490f00000f"),
      "completionDate": ISODate("2013-02-28T04:06:08.18Z"),
      "complete": true
    },
    {
      "todo": "drag drop into project",
      "_id": ObjectId("510417455467feb313000004"),
      "completionDate": ISODate("2013-02-28T04:06:08.18Z"),
      "complete": true
    },
    {
      "todo": "make statuses clickable",
      "_id": ObjectId("50e9ec22067294a60b000002"),
      "completionDate": ISODate("2013-02-28T04:06:08.18Z"),
      "complete": true
    },
    {
      "todo": "UI representation of that",
      "_id": ObjectId("50ddb718e7a4797d23000015"),
      "completionDate": ISODate("2013-02-28T04:06:08.18Z"),
      "complete": true
    },
    {
      "todo": "Grouping (map reduce)",
      "_id": ObjectId("50ddb718e7a4797d23000016"),
      "completionDate": ISODate("2013-02-28T04:06:08.18Z"),
      "complete": true
    }
  ],
  "title": "Standard Grouping of Tasks"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50aee29b83629c8323000005"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-01-06T22:48:00.924Z"),
  "description": "Task dialog shared by new and update task, same template.",
  "group": "UI\/UX",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(19),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-11-18T06:04:08.272Z"),
  "tasks": [
    {
      "todo": "create new task form template",
      "_id": ObjectId("50aee29b83629c8323000008"),
      "completionDate": ISODate("2013-02-19T03:53:32.755Z"),
      "complete": true
    },
    {
      "todo": "create new task view",
      "_id": ObjectId("50aee29b83629c8323000007"),
      "completionDate": ISODate("2013-02-19T03:53:32.755Z"),
      "complete": true
    },
    {
      "todo": "hookup with modal dialog",
      "_id": ObjectId("50d29d0e6dabfcd40f000004"),
      "completionDate": ISODate("2013-02-19T03:53:32.755Z"),
      "complete": true
    }
  ],
  "title": "Make New Task dialog"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510b400ab0d29f900c000004"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-02-21T04:30:30.26Z"),
  "description": "test",
  "group": "",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(1),
  "project": ObjectId("50f6117702f7488108000004"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-01T04:09:33.154Z"),
  "tasks": [
    {
      "todo": "touch down",
      "_id": ObjectId("510b4040b0d29f900c000005"),
      "completionDate": ISODate("2013-02-21T04:30:30.26Z"),
      "complete": true
    }
  ],
  "title": "test"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510b405db0d29f900c000006"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "",
  "group": "so",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(5),
  "project": ObjectId("50f7767551a5d5450c000004"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-01T04:09:33.154Z"),
  "tasks": [
    {
      "todo": "up",
      "_id": ObjectId("510b405db0d29f900c000008"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "down up",
      "_id": ObjectId("510b405db0d29f900c000007"),
      "completionDate": ISODate("2013-02-23T19:15:34.130Z"),
      "complete": true
    },
    {
      "todo": "and away",
      "_id": ObjectId("510b405db0d29f900c000009"),
      "completionDate": ISODate("2013-02-23T19:15:34.130Z"),
      "complete": true
    }
  ],
  "title": "more test"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510d4e39b0d29f900c000013"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-02-02T18:13:57.679Z"),
  "description": "now",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(2),
  "project": ObjectId("50f6117702f7488108000004"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-02T17:28:09.344Z"),
  "tasks": [
    {
      "todo": "gone",
      "_id": ObjectId("510d4e39b0d29f900c000014"),
      "completionDate": ISODate("2013-02-02T19:05:42.599Z"),
      "complete": true
    }
  ],
  "title": "leave"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510d4ec4b0d29f900c000015"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "1",
  "group": "",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(3),
  "project": ObjectId("50f6117702f7488108000004"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-02T17:28:09.344Z"),
  "tasks": [
    {
      "todo": "2",
      "_id": ObjectId("510d4ec4b0d29f900c000016"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "wowzers"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510d580f7a5f0d9b0d000004"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-02-26T05:14:42.530Z"),
  "description": "",
  "group": "",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(2),
  "project": ObjectId("50f6117702f7488108000004"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-02T18:16:01.292Z"),
  "tasks": [
    {
      "todo": "finish",
      "_id": ObjectId("510d580f7a5f0d9b0d000005"),
      "completionDate": ISODate("2013-02-26T05:14:42.529Z"),
      "complete": true
    }
  ],
  "title": "new 2"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510d59817a5f0d9b0d000006"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Crazy stuff",
  "group": "something",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(2),
  "project": ObjectId("50f7767551a5d5450c000004"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-02T18:17:10.714Z"),
  "tasks": [
    {
      "todo": "present",
      "_id": ObjectId("510d59817a5f0d9b0d000008"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "gift",
      "_id": ObjectId("510d59817a5f0d9b0d000007"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Weird"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510d63ebe0b944ea0d000004"),
  "activities": [
    
  ],
  "completionDate": null,
  "description": "2hours",
  "group": "",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(4),
  "project": ObjectId("50f7767551a5d5450c000004"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-02T19:06:31.65Z"),
  "tasks": [
    {
      "todo": "1 hour",
      "_id": ObjectId("510d63ebe0b944ea0d000005"),
      "complete": false
    }
  ],
  "title": "8 cases"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510d655be0b944ea0d000006"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "banks",
  "group": "",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(3),
  "project": ObjectId("50f7767551a5d5450c000004"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-02T19:13:07.541Z"),
  "tasks": [
    {
      "todo": "no show",
      "_id": ObjectId("510d655be0b944ea0d000007"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "too cheap"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510d6596e0b944ea0d000008"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-02-02T19:14:41.320Z"),
  "description": "more",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(1),
  "project": ObjectId("50f7767551a5d5450c000004"),
  "status": "complete",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-02T19:13:07.541Z"),
  "tasks": [
    {
      "todo": "life",
      "_id": ObjectId("510d6596e0b944ea0d00000a"),
      "completionDate": ISODate("2013-02-02T19:14:41.319Z"),
      "complete": true
    },
    {
      "todo": "took",
      "_id": ObjectId("510d6596e0b944ea0d000009"),
      "completionDate": ISODate("2013-02-02T19:14:41.319Z"),
      "complete": true
    }
  ],
  "title": "More and more"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510db658e0b944ea0d00000c"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Ability to add comments to projects and tasks (maybe the checklist also?)",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(9),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-03T00:54:38.280Z"),
  "tasks": [
    {
      "todo": "comment data structure",
      "_id": ObjectId("510db658e0b944ea0d00000d"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "backbone model",
      "_id": ObjectId("510dba17e0b944ea0d000011"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "comment  api",
      "_id": ObjectId("510db658e0b944ea0d00000e"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "comment template",
      "_id": ObjectId("510dba17e0b944ea0d000010"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "collection and view hook up",
      "_id": ObjectId("510dba17e0b944ea0d00000f"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Comments and notes"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510dcfe3e0b944ea0d000012"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Make task as part of project model",
  "group": "Model",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(15),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-03T02:44:33.41Z"),
  "tasks": [
    {
      "todo": "extend project model to include task object",
      "_id": ObjectId("510dcfe3e0b944ea0d000016"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "migrate task objects to projects tasks",
      "_id": ObjectId("510dcfe3e0b944ea0d000015"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "update api",
      "_id": ObjectId("510dcfe3e0b944ea0d000014"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "update backbone and ui",
      "_id": ObjectId("510dcfe3e0b944ea0d000013"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Restructure task and project data model"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50ccbcbde5a6baf40a000002"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Tile design, tile animations, tile colors. How can these tiles appeal more and make users want to always add more tasks\n(https:\/\/wrapbootstrap.com\/themes\/admin\/page.1\/sort.sales\/order.desc)",
  "group": "UI\/UX",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(7),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    "UI",
    "UX"
  ],
  "task": [
    
  ],
  "taskDate": ISODate("2012-12-15T16:44:40.23Z"),
  "tasks": [
    {
      "todo": "make a more solid app UI ",
      "_id": ObjectId("510dd0e9e0b944ea0d000018"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "auto expand on '...' to show more information",
      "_id": ObjectId("50e203a60f8c752727000026"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "create tagging ui",
      "_id": ObjectId("511819d31f5f5e7b08000004"),
      "completionDate": ISODate("2013-02-24T00:25:27.499Z"),
      "complete": true
    },
    {
      "todo": "subtasks entry change, have empty input and listen to enter",
      "_id": ObjectId("5115c82ac8f2f1490f00001e"),
      "completionDate": ISODate("2013-02-24T00:25:27.499Z"),
      "complete": true
    },
    {
      "todo": "efficient loading of tiles in an animated fashion",
      "_id": ObjectId("50d3aedb6dabfcd40f00000c"),
      "completionDate": ISODate("2013-02-24T00:25:27.499Z"),
      "complete": true
    },
    {
      "todo": "limit the amount of lines for each tile",
      "_id": ObjectId("50dcd9a5cef71be220000003"),
      "completionDate": ISODate("2013-02-24T00:25:27.499Z"),
      "complete": true
    },
    {
      "todo": "tiling arrangement, optimize for space",
      "_id": ObjectId("50d3aedb6dabfcd40f00000b"),
      "completionDate": ISODate("2013-02-24T00:25:27.499Z"),
      "complete": true
    }
  ],
  "title": "UX\/UI"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510df33fc8f2f1490f000004"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Master bedroom make over",
  "group": "bedroom",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(1),
  "project": ObjectId("510de7e2e0b944ea0d000019"),
  "status": "ongoing",
  "tags": [
    "painting",
    "white paint"
  ],
  "taskDate": ISODate("2013-02-03T05:17:19.172Z"),
  "tasks": [
    {
      "todo": "find art for center and focal point of the room",
      "_id": ObjectId("510df33fc8f2f1490f000008"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "find painting for left and right of bed",
      "_id": ObjectId("510df33fc8f2f1490f000007"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "paint walls",
      "_id": ObjectId("510df33fc8f2f1490f000006"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "paint ceiling",
      "_id": ObjectId("510df33fc8f2f1490f000005"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Bedroom make over"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("510df38fc8f2f1490f000009"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Main bathroom make over",
  "group": "bathroom",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(2),
  "project": ObjectId("510de7e2e0b944ea0d000019"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-03T05:17:19.172Z"),
  "tasks": [
    {
      "todo": "Fix leaky bath tub faucet",
      "_id": ObjectId("510df38fc8f2f1490f00000e"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "paint doors",
      "_id": ObjectId("510df38fc8f2f1490f00000d"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "paint cupboards ",
      "_id": ObjectId("510df38fc8f2f1490f00000c"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "paint walls",
      "_id": ObjectId("510df38fc8f2f1490f00000b"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "replace toilet seat",
      "_id": ObjectId("510df38fc8f2f1490f00000a"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Main bathroom make over"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("51158e76c8f2f1490f000010"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Reorganize and paint",
  "group": "basement",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(3),
  "project": ObjectId("510de7e2e0b944ea0d000019"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-08T03:57:51.806Z"),
  "tasks": [
    {
      "todo": "separate bronx room and laundry area ",
      "_id": ObjectId("51158e76c8f2f1490f000015"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "new washer and dryer",
      "_id": ObjectId("51158e76c8f2f1490f000014"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "re-organize",
      "_id": ObjectId("51158e76c8f2f1490f000013"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "paint walls",
      "_id": ObjectId("51158e76c8f2f1490f000012"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "paint floor",
      "_id": ObjectId("51158e76c8f2f1490f000011"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Laundry room \/ Bronx room"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("5115c7b3c8f2f1490f000016"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Activities of tasks and projects",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(8),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-09T03:46:37.180Z"),
  "tasks": [
    {
      "todo": "asynchronous flow",
      "_id": ObjectId("5115c7b3c8f2f1490f000019"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "publish\\subscribe",
      "_id": ObjectId("5115c7b3c8f2f1490f000018"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "model",
      "_id": ObjectId("5115c7b3c8f2f1490f000017"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "History"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("5115c8edc8f2f1490f00001f"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Creating a gym \/ movie room",
  "group": "basement",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(4),
  "project": ObjectId("510de7e2e0b944ea0d000019"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-02-09T03:46:37.180Z"),
  "tasks": [
    {
      "todo": "organization and storage",
      "_id": ObjectId("5115c8edc8f2f1490f000022"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "clean up",
      "_id": ObjectId("5115c8edc8f2f1490f000021"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "paint walls",
      "_id": ObjectId("5115c8edc8f2f1490f000020"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Basement"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("50d6116d73be325515000015"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Cloning of tasks (to clone on server or to clone on client)\nclone should ensure all subtasks are not",
  "group": "Application",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(13),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2012-12-22T19:58:39.962Z"),
  "tasks": [
    {
      "todo": "clone method",
      "_id": ObjectId("50d6116d73be325515000017"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "subtask updates during clone",
      "_id": ObjectId("50d6116d73be325515000016"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Clone feature"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("5122ca6abd9053e20b000004"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Deception is a complex show",
  "group": "outsider",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(3),
  "project": ObjectId("50f6117702f7488108000004"),
  "status": "ongoing",
  "tags": [
    "what",
    "happended"
  ],
  "taskDate": ISODate("2013-02-19T00:23:04.227Z"),
  "tasks": [
    {
      "todo": "and then some",
      "_id": ObjectId("5122ca6abd9053e20b000007"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "and more",
      "_id": ObjectId("5122ca6abd9053e20b000006"),
      "completionDate": ISODate("2013-02-26T05:14:34.588Z"),
      "complete": true
    },
    {
      "todo": "a lot",
      "_id": ObjectId("5122ca6abd9053e20b000005"),
      "completionDate": ISODate("2013-02-26T05:14:34.588Z"),
      "complete": true
    }
  ],
  "title": "This is a nothing"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("5122f99bbd9053e20b000008"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-02-23T19:03:46.234Z"),
  "description": "Everything and anything to do with grouping of tasks within the UI.",
  "group": "UI\/UX",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(1),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "complete",
  "tags": [
    "grouping",
    "UI",
    "groups",
    "group",
    "filter"
  ],
  "taskDate": ISODate("2013-02-19T01:46:58.627Z"),
  "tasks": [
    {
      "todo": "autocomplete for group",
      "_id": ObjectId("51290524bd9053e20b000018"),
      "completionDate": ISODate("2013-02-23T19:03:46.234Z"),
      "complete": true
    },
    {
      "todo": "need a clear button to clear group filter",
      "_id": ObjectId("5122f99bbd9053e20b00000a"),
      "completionDate": ISODate("2013-02-23T19:03:46.234Z"),
      "complete": true
    },
    {
      "todo": "filter based on groups",
      "_id": ObjectId("5122f99bbd9053e20b000009"),
      "completionDate": ISODate("2013-02-23T19:03:46.234Z"),
      "complete": true
    },
    {
      "todo": "fix tagging issue on new task",
      "_id": ObjectId("5122f99bbd9053e20b00000b"),
      "completionDate": ISODate("2013-02-23T19:03:46.234Z"),
      "complete": true
    },
    {
      "todo": "fix grouping on new task",
      "_id": ObjectId("5122fa2fbd9053e20b00000d"),
      "completionDate": ISODate("2013-02-23T19:03:46.234Z"),
      "complete": true
    }
  ],
  "title": "Grouping UI"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("5122fb7cbd9053e20b00000e"),
  "activities": [
    
  ],
  "complete": true,
  "completionDate": ISODate("2013-02-26T05:14:40.141Z"),
  "description": "Praise God",
  "group": "Praises",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(1),
  "project": ObjectId("50f6117702f7488108000004"),
  "status": "complete",
  "tags": [
    "Alive"
  ],
  "taskDate": ISODate("2013-02-19T04:10:44.695Z"),
  "tasks": [
    {
      "todo": "Shame",
      "_id": ObjectId("5122fb7cbd9053e20b000010"),
      "completionDate": ISODate("2013-02-26T05:14:40.141Z"),
      "complete": true
    },
    {
      "todo": "No bail",
      "_id": ObjectId("5122fb7cbd9053e20b00000f"),
      "completionDate": ISODate("2013-02-26T05:14:16.898Z"),
      "complete": true
    }
  ],
  "title": "Alive"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("512435c4bd9053e20b000014"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Stacked view of tasks",
  "group": "View",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(4),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    "view",
    "stacked view"
  ],
  "taskDate": ISODate("2013-02-19T04:13:45.520Z"),
  "tasks": [
    {
      "todo": "JS to manage view",
      "_id": ObjectId("512435c4bd9053e20b000016"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "Backbone view ",
      "_id": ObjectId("512435c4bd9053e20b000015"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Stacked Tile View"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("5122fc7cbd9053e20b000012"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Any UI Defects",
  "group": "Defect",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(6),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    "UI"
  ],
  "taskDate": ISODate("2013-02-19T04:13:45.520Z"),
  "tasks": [
    {
      "todo": "UI cleanup needed when in project view",
      "completionDate": null,
      "_id": ObjectId("51295dcfbd9053e20b00001a"),
      "complete": false
    },
    {
      "todo": "drag and drop issue with tasks into project",
      "_id": ObjectId("512915a3bd9053e20b000019"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "group autocomplete item focus color",
      "_id": ObjectId("5128f919bd9053e20b000017"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "Fix deleting from task dialog",
      "_id": ObjectId("5122fca3bd9053e20b000013"),
      "completionDate": ISODate("2013-02-24T00:24:47.173Z"),
      "complete": true
    }
  ],
  "title": "UI Defects"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("5129a754c8a599e30e000004"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Project tiles act as project dashboard, should contain some summary, ability to add task ...",
  "group": "Project UI",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(1),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    "project",
    "ui",
    "dashboard"
  ],
  "taskDate": ISODate("2013-02-24T05:25:37.505Z"),
  "tasks": [
    {
      "todo": "add task button",
      "_id": ObjectId("5129a803c8a599e30e000005"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "task summary",
      "_id": ObjectId("5129a803c8a599e30e000006"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "tasks stats  -how many groups",
      "_id": ObjectId("5129a803c8a599e30e000008"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "tasks stats - percentage of completeness",
      "_id": ObjectId("5129a803c8a599e30e000007"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "tasks stats - distribution",
      "_id": ObjectId("5129a803c8a599e30e000009"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "possible charts of progress",
      "_id": ObjectId("5129a803c8a599e30e00000a"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "Project dashboard"
});
db.getCollection("tasks").insert({
  "__v": NumberInt(0),
  "_id": ObjectId("513112e85098b0fd08000004"),
  "activities": [
    
  ],
  "complete": false,
  "completionDate": null,
  "description": "Any any enhancements related to UI",
  "group": "Ehancement",
  "owner": ObjectId("5084f6a585b676bb51000001"),
  "priority": NumberInt(2),
  "project": ObjectId("50f99d0d51a5d5450c000006"),
  "status": "ongoing",
  "tags": [
    
  ],
  "taskDate": ISODate("2013-03-01T02:17:09.796Z"),
  "tasks": [
    {
      "todo": "add scrolling to subtasks",
      "_id": ObjectId("513112e85098b0fd08000005"),
      "completionDate": null,
      "complete": false
    },
    {
      "todo": "add scrolling to groups",
      "_id": ObjectId("5131187f5098b0fd08000006"),
      "completionDate": null,
      "complete": false
    }
  ],
  "title": "UI Enhancements"
});
