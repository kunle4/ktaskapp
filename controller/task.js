var http = require('http');
var q = require('q');
var app = require('express')();
var TaskSchema = require('./../model/task');
var ProjectSchema = require('./../model/project');
var taskManager = require('./../service/taskManager');
var dbName = 'ktask';
var collectionName = 'tasks';

var mongoose = require('mongoose');
//var connection = mongoose.connect('mongodb://localhost:27017/' + dbName);
var Task = mongoose.model('Task', TaskSchema);
var Project = mongoose.model('Project', ProjectSchema);

var sys = require("util");  
var test = require("assert");  

//sys.puts("Using mongodb://localhost:27017/");  

function dumpProps(obj) {
   // Go through all the properties of the passed-in object 
	var msg = "{";
	for (var i in obj) {
	   msg  += i + ":";
	   if(typeof obj[i] == "object")
		   msg+=dumpProps(obj[i]);
	   else
		   msg += obj[i]+",";
	}
	msg += "}";
	return msg;
}
var getTasksHtml = function(req, res){
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write('<html><body style="background-color:#000000;color:#FFFFFF">');
	res.write('<h1>Todo list</h1>'); 
	Task.findTasksByUser(req.session.user._id, function (err, results){  
		if(results.length > 0 ){
			for(var i=0; i< results.length; i++){
				res.write('title: ');
				res.write(results[i].title);
				res.write('<br/>');

				res.write('description: ');
				res.write(results[i].description);
				res.write('<br/>');

				res.write('note: ');
				//res.write(results[i].note);
				res.write('<br/>');

				if(results[i].tags.length > 0){
					res.write('tags: ');
					for(var j = 0 ; j< results[i].tags.length ; j++)
						res.write(results[i].tags[j] +',');
					res.write('<br/><br/>');
				}
			}	
		}
		res.end('</body></html>'); 
	});
};

var getTasks = function(req, res){
	console.log('Searching for tasks for ' + req.session.user.username + '...');
	var filter = req.query;
	filter.owner = req.session.user._id;
	console.log(filter);
	Task.findTasksByUser(filter,function(err,tasks){
		//res.json(tasks);	
		//res.write(JSON.stringify(taskManager.groupTasksByDate(tasks)));
		console.log(tasks);
		if(err)
			console.log(err);
		res.write(JSON.stringify(tasks));
		res.end();		
	});
};


var getTasksByTitle = function(title,req, res){
	Task.findByTitle(title, function(err,tasks){
		res.send(tasks);
		res.write(JSON.stringify(tasks));
		res.end();
	});
};

var addTask = function(res, taskJson){
	console.log("Adding task :" + taskJson); 
	var task = new Task(taskJson);
	getLowestProrityTask(task, function(err,lTasks){
		console.log('The type of returned is = ' + typeof lTasks);
		if(lTasks !== null && typeof lTasks !== "undefined" && typeof lTasks[0] !== "undefined") {
			task.priority = lTasks[0].priority + 1;
		}else
			task.priority = 1;
			task.save(function(err, task){
			console.log('added: ' + task);
			res.write(JSON.stringify(task));
			res.end();
		});	
	});                   
};

var removeTask = function(res, id){
	Task.findById(id, function(err, task){
		console.log('removing: ' + task);
		task.remove();
		var isRemoved = true;
		res.write(JSON.stringify(isRemoved));
		res.end();
	});                      
};

var _isSubtasksComplete = function (task){
	var isComplete = false;
	  console.log('In the isSubtasksComplete');
	  if(task.tasks.length > 0){
	    var todos = task.tasks;
	    isComplete = true;

	    for (var i = todos.length - 1; i >= 0; i--) {
	      if(!todos[i].complete){
	        isComplete = false;
	        break;
	      }
	    }
	  }
	  console.log(isComplete);  
	  return isComplete;
};

var _processSubtasks = function (task){
	console.log('In the _processSubtasks');
	if(task.tasks.length > 0){
		var todos = task.tasks;

		for (var i = todos.length - 1; i >= 0; i--) {
			if(todos[i].complete)
		    	todos[i].completionDate = todos[i].completionDate ==null ? new Date() : todos[i].completionDate;
		    else
		    	todos[i].completionDate = null;	  
		}
	} 
	return;
};

var getLowestProrityTask = function (task, callback){
	var filter = {};
	filter.owner = task.owner;
	filter.project = typeof task['project']=== "undefined" ? null : task.project; 

	Task.findLowestPrority(filter,callback);
};

var updateTasksPriority = function (res, data){
	var count = 0;
	var p = 0;
	var resData = [];
	for (var i =0; i < data.length; i++) {
   		p++;
		Task.update({_id: data[i]},{priority:p},{upsert:false}, function(err){
			resData[count] = data[count];
			//console.log(data[count]);
			count++;			
			
			if(err != null){
				console.log(err);
			}
			if( count == data.length ){  
				res.write(JSON.stringify(resData));
				res.end();
			}
		});
	};	
};
var updateTasks = function(res, tasks){
	//var priority = 0;
	var count = 0;
	console.log('in the task controller for update tasks');
	/*getLowestProrityTask(tasks[0], function(err,lTasks){
		console.log('The type of returned is = ' + typeof lTasks);
		if(lTasks !== null && typeof lTasks !== "undefined" && typeof lTasks[0] !== "undefined") 
			priority = lTasks[0].priority + 1;
		else
			priority = 1;*/
		for (var i = 0; i < tasks.length; i++) {
			var task = tasks[i];
			task.id = task._id;
    		delete task._id;
			
			//task.priority = priority;
			Task.update({_id: task.id}, task, {upsert: false}, function(err){
				if (err) console.log(err);
				count ++;
				console.log(count);
		        /*Task.findOne({ _id: task._id}, function (err, _task) {
		  			if (err) console.log(err);
		  			console.log(_task);
		  			var isUpdated = true;
			        var data = {isUpdated:isUpdated, task:_task}
			        res.write(JSON.stringify(data));
			        res.end();
				});	*/
				//done updating  
				if(count == tasks.length){
					var data = {allUpdated:true}
			        res.write(JSON.stringify(data));
			        res.end();
				}      
			});
			//priority++;
		};
	//});
};

var updateTask = function(res,task){
    console.log("updating task :" + task._id);
    //need to remove the _id property before saving because of mongo conflict
    task.id = task._id;
    delete task._id;
    _processSubtasks(task);
    task.complete = _isSubtasksComplete(task);
    task.status = task.complete == true ? 'complete' : 'ongoing';
    if(task.complete)
    	task.completionDate = task.completionDate == null ? new Date() : task.completionDate;
	else
		task.completionDate = null;
    
    Task.update({_id: task.id}, task, {upsert: true}, function(err){
        Task.findOne({ _id: task.id}, function (err, _task) {
  			if (err) console.log(err);
  			console.log(_task);
  			var isUpdated = true;
	        var data = {isUpdated:isUpdated, task:_task}
	        res.write(JSON.stringify(data));
	        res.end();
		});	        
	});
};

var completeTask = function(res, task){
	for (var i = task.tasks.length - 1; i >= 0; i--) {
		if(!task.tasks[i].complete)
			task.tasks[i].complete = true;
	};
	updateTask(res,task);
};

var getStatuses = function(res, req){
	var projectId = typeof req.query.project === "undefined" 
					|| req.query.project == "null" 
					|| req.query.project == "undefined" 
					? null : req.query.project;
	getStatusesByProject(req.session.user._id, projectId).then(function(value){
		res.write(JSON.stringify(value));
		res.end();
	});
};

var getAllStatuses = function(res, req){
	Project.findProjectsByUser(req.session.user._id,function(err,projects){
		
		var promises = [];

		for (var i = 0; i < projects.length; i++) {
			promises.push(getStatusesByProject(req.session.user._id, projects[i]._id));
		}
		console.log('the promises: ',promises);
		q.allResolved(promises)
		.then(function (promises) {
			var i =0;
			var projectStatuses = {};
		    promises.forEach(function (promise) {
		    	//console.log('in the promise foreach');
		    	i++;
		    	//console.log('promise count',i);
		        if (promise.isFulfilled()) {
		            var value = promise.valueOf();
		            try{
		            	//console.log('the val from the promise',value);
		            	projectStatuses[value[0].projectId]=value;		            
		            	if(i ==promises.length ){
				            res.write(JSON.stringify(projectStatuses));
				            res.end();
			        	}
		        	}catch(e){
		        		console.log(e);
		        	}
		        } 
		    })
		});	
	});
};

var getStatusesByProject = function(userId, projectId){
	var options = {};
	var deferred = q.defer();
	options.map = function () { emit(this.status, 1) };
	options.reduce = function (key, vals) {
		var count = 0;
		return vals.length; 
	};

	options.query = {owner:userId, project: projectId == null ? { $exists: false } : projectId};
	Task.mapReduce(options, function (err, results) {
		if(err)
			console.log(err);
		var statuses = [];
		console.log(results);
		if(typeof results !== "undefined"){
			for (var i = results.length - 1; i >= 0; i--) {
				results[i].status = results[i]._id;
				results[i].projectId = projectId;
				delete results[i]._id;
				statuses[i] = results[i];
			};
			deferred.resolve(statuses);
		}		
	});
	return deferred.promise;
};


var getTaskCountPerProject = function(res, userId){
	var options = {};
	options.map = function () { emit(this.project, 1) };
	options.reduce = function (key, vals) {
		var count = 0;
		return vals.length; 
	};
	options.query = {owner:userId};
	Task.mapReduce(options, function (err, results) {
		if(err)
			console.log(err);
		var counts = [];
		console.log(results);
		if(typeof results !== "undefined"){
			for (var i = results.length - 1; i >= 0; i--) {
				results[i].projectId = results[i]._id;
				delete results[i]._id;
				counts[i] = results[i];
			};
		}
		
		res.write(JSON.stringify(counts));
		res.end();
	});
};

var getGroupFacets = function(res, req){
	var options = {};
	var projectId = typeof req.query.project === "undefined" 
				|| req.query.project == "null" 
				|| req.query.project == "undefined" 
				? null : req.query.project;

	var status = typeof req.query.status === "undefined" 
				|| req.query.status == "null" 
				|| req.query.status == "undefined" 
				? null : req.query.status;	

	var userId = req.session.user._id;	

	options.map = function () { emit(this.group, 1) };
	options.reduce = function (key, vals) {
		var count = 0;
		return vals.length; 
	};

	if(status==null){
		options.query = {owner:userId, 
						project: projectId == null ? { $exists: false } : projectId};
	}else{
		options.query = {owner:userId, 
						project: projectId == null ? { $exists: false } : projectId, 
						status: status == null ? { $exists: false } : status};
	}

	Task.mapReduce(options, function (err, results) {
		if(err)
			console.log(err);
		var statuses = [];
		console.log(results);
		if(typeof results !== "undefined"){
			for (var i = results.length - 1; i >= 0; i--) {
				results[i].group = results[i]._id;
				delete results[i]._id;
				statuses[i] = results[i];
			};
		}
		
		res.write(JSON.stringify(statuses));
		res.end();
	})
};

exports.getTasks = getTasks;
exports.addTask = addTask;
exports.removeTask = removeTask;
exports.getTasksByTitle = getTasksByTitle;
exports.getTasksHtml = getTasksHtml;
exports.updateTask = updateTask;
exports.updateTasksPriority = updateTasksPriority;
exports.updateTasks = updateTasks;
exports.getStatuses = getStatuses;
exports.getAllStatuses = getAllStatuses;
exports.getTaskCountPerProject = getTaskCountPerProject;
exports.getGroupFacets = getGroupFacets;
exports.completeTask = completeTask;