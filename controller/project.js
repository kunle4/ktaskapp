var http = require('http');
var q = require('q');
var app = require('express')();
var ProjectSchema = require('./../model/project');
var TaskSchema = require('./../model/task');
var _ = require('underscore');
var activityService = require('./../service/activityService');
var dbName = 'ktask';

var mongoose = require('mongoose');
var Project = mongoose.model('Project', ProjectSchema);
var Task = mongoose.model('Task', TaskSchema);

var sys = require("util");  
var test = require("assert");  

var getProjects = function(req, res){
	console.log('Searching for projects for ' + req.session.user.username + '...');
	Project.findProjectsByUser(req.session.user._id,function(err,projects){
        //add some stats to project node
        _.each(projects, function(project){
            project.statistics = _getStats(project);
        });
		res.write(JSON.stringify(projects));
		res.end();		
	});
};

var _getStats = function(project){
    var stats = {};
    stats.tasksCount = project.tasks.length;
    stats.tasksCompleted = _.filter(project.tasks, function(task){ return task.complete;}).length;
    stats.percentCompleted = stats.tasksCount== 0 ? 0 : ((stats.tasksCompleted/stats.tasksCount) * 100).toFixed(0);
    return stats;
}

var addProject = function(res, projectJson){
	console.log("Adding project :" + projectJson); 
	var project = new Project(projectJson);
	project.activities = [];
	project.activities.push(activityService.getCreateActivity(project.owner, {objectType:"project"}));
	project.save(function(err, project){
		console.log('added: ' + project);
		res.write(JSON.stringify(project));
		res.end();
	});	                   
};

var addTask = function(res, taskJson, projectId){
    //console.log("Adding task :" + JSON.stringify(taskJson));
    var task = new Task(taskJson);
    Project.findOne({_id:projectId}, function(err,project){
        if(err)
            console.log(err);

        var lTask = {};
        if(project.tasks.length > 0) {
            var ongoingTasks = [];
            for(var i = 0; i < project.tasks.length; i++){
                if(!project.tasks[i].complete)
                    ongoingTasks.push(project.tasks[i]);
            }
            lTask = ongoingTasks[ongoingTasks.length-1];
        }
        if(lTask.priority === undefined)
            task.priority = 1;
        else
            task.priority = lTask.priority + 1;
        task._id = new mongoose.Types.ObjectId();

        console.log('The task to save:' + JSON.stringify(task));
        project.tasks.push(task);
        project.activities.push(activityService.getUpdateActivity(project.owner, {objectType:"project", id:project._id}));

        /*Project.update({id: project._id}, {$set:{tasks: project.tasks}}, function(err){
            if(err)
                console.log(err);
            var isUpdated = true;
            var data = {isUpdated:isUpdated, task:task}
            res.write(JSON.stringify(data));
            res.end();
        });*/
        project.save();

        var isUpdated = true;
        var data = {isUpdated:isUpdated, task:task}
        res.write(JSON.stringify(data));
        res.end()
    });
};

var _processSubtasks = function (task){
    if(task.tasks.length > 0){
        var todos = task.tasks;

        for (var i = todos.length - 1; i >= 0; i--) {
            if(todos[i].complete)
                todos[i].completionDate = todos[i].completionDate ==null ? new Date() : todos[i].completionDate;
            else
                todos[i].completionDate = null;
        }
    }
    return;
};

var _isSubtasksComplete = function (task){
    var isComplete = false;
    if(task.tasks.length > 0){
        var todos = task.tasks;
        isComplete = true;

        for (var i = todos.length - 1; i >= 0; i--) {
            if(!todos[i].complete){
                isComplete = false;
                break;
            }
        }
    }
    console.log(isComplete);
    return isComplete;
};

var updateTask = function(res, task, projectId){
    console.log("updating task :" + task._id);
    _processSubtasks(task);
    task.complete = _isSubtasksComplete(task);
    task.status = task.complete == true ? 'complete' : 'ongoing';
    if(task.complete)
        task.completionDate = task.completionDate == null ? new Date() : task.completionDate;
    else
        task.completionDate = null;

    Project.findOne({_id: projectId}, function(err, project){
        if (err) console.log(err);
        for(var i = 0; i < project.tasks.length; i++){
            if(project.tasks[i]._id){
                if(project.tasks[i]._id.toString() == task._id.toString()){
                    project.tasks[i] = task;
                    break;
                }
            }
        };

        Project.update({_id: projectId}, {tasks: project.tasks}, function(err){
           if(err) console.log(err);

            var isUpdated = true;
            var data = {isUpdated:isUpdated, task:task}
            res.write(JSON.stringify(data));
            res.end();
        });
    });
};

var removeTask = function(res, taskId, projectId){
    console.log("Removing task :" + taskId);

    Project.findOne({_id: projectId}, function(err, project){
        if (err) console.log(err);
        var updatedTasks = _.filter(project.tasks, function(task){
           return task._id != taskId;
        });

        project.tasks = updatedTasks;

        Project.update({_id: projectId}, {tasks: project.tasks}, function(err){
            if(err) console.log(err);

            var isRemoved = true;
            res.write(JSON.stringify(isRemoved));
            res.end();
        });
    });
};

var getTasks = function(res, projectId, filter){
    console.log('Fetching tasks for ' + projectId + '...where : ' + JSON.stringify(filter));
    Project.getTasks(projectId,function(err,tasks){
        if(err)
            console.log(err);
        if(filter.group){
            filter.group = JSON.parse(filter.group);
        }
        //var filteredTasks =_.where(tasks, filter);
        var filteredTasks =_.filter(tasks, function(task){
            var matchCount = 0;
            var filterCount = 0;
            for(prop in filter){
                filterCount++;
                if(filter[prop]['splice'] === undefined && task[prop] == filter[prop]){
                    matchCount ++;
                }else if(filter[prop]['splice'] !== undefined){
                    for(var i=0; i < filter[prop].length; i++){
                        if(task[prop] == filter[prop][i]) {
                            matchCount++;
                        }
                    }
                }
            }
            return matchCount == filterCount;
        });
        filteredTasks = _.sortBy(filteredTasks, 'priority');
        res.write(JSON.stringify(filteredTasks));
        res.end();
    });
};

var getLowestProrityTask = function (projectId, callback){
    Project.findTaskWithLowestPriority(projectId,callback);
};

var removeProject = function(res, id){
    console.log('removing project with Id: ' + id);
	Project.removeProject(id, function(err){
		var isRemoved = true;
		res.write(JSON.stringify(isRemoved));
		res.end();
	});                      
};

var getTasksByProject = function(projectId){
    var deferred = q.defer();
    Task.findByProject(projectId, function(err, tasks){
        if(err)
            console.log(err);
        console.log(JSON.stringify(tasks));
        deferred.resolve(tasks);
    });
    return deferred.promise;
};

var updateProjectTemp = function(res,project){
    console.log("updating project :" + project._id);
    //need to remove the _id property before saving because of mongo conflict
    project.id = project._id;
    delete project._id;
    project.activities.push(activityService.getUpdateActivity(project.owner, {objectType:"project", id:project.id}));
    Project.update({_id: project.id}, project, function(err){
        console.log('maybe error: ', err);
        Project.findOne({ _id: project.id}, function (err, _project) {
            if (err) console.log(err);
            //console.log(_project);
            var isUpdated = true;
            var data = {isUpdated:isUpdated, project:_project}
            res.write(JSON.stringify(data));
            res.end();
        });
    });


};

var updateProject = function(res,project){
    console.log("updating project :" + project._id);
    //need to remove the _id property before saving because of mongo conflict
    project.id = project._id;
    delete project._id;
    getTasksByProject(project.id).then(function(tasks){
        console.log('in the then with project:',project.id);
        project.tasks = tasks;

        project.activities.push(activityService.getUpdateActivity(project.owner, {objectType:"project", id:project.id}));
        Project.update({_id: project.id}, project, {upsert: true}, function(err){
            Project.findOne({ _id: project.id}, function (err, _project) {
                if (err) console.log(err);
                console.log(_project);
                var isUpdated = true;
                var data = {isUpdated:isUpdated, project:_project}
                res.write(JSON.stringify(data));
                res.end();
            });
        });
    });

};

var getGroupFacets = function(res, projectId, filter){
    var options = {};

    //create {value: count, group: name}

    Project.findOne({_id: projectId}, function (err, project) {
        if(err)
            console.log(err);
        var groups = [];
        var tasks = _.where(project.tasks, filter);
        var groupings = _.countBy(tasks, "group");
        for(var prop in groupings){
            groups.push({value: groupings[prop], group:prop});
        }

        res.write(JSON.stringify(groups));
        res.end();
    })
};

var getTaskCountPerProject = function(res, userId){
    //create {value: count, projectId: id}

    Project.findProjectsByUserLean(userId, '_id tasks title', function (err, projects) {
        if(err)
            console.log(err);
        //count for each task
        var countByTask = [];
        _.each(projects, function(project){
            countByTask.push({value: project.tasks.length, projectId:project._id, name: project.title});
        });

        res.write(JSON.stringify(countByTask));
        res.end();
    })
};

var updateTasksPriority = function (res, projectId, data){
    var resData = [];
     console.log('going to prioritize with', data);
    Project.findProjectsById(projectId, function(err, project){
        if(err != null){
            console.log(err);
        }
        var count = 0;
        var p = 0;
        _.each(data, function(id){

            var priority = count+1;
            var task = _.find(project.tasks, function(task){
               return task._id ==  id;
            });
            if(task) {
                task.priority = priority;
                resData[priority-1] = task._id;
            }
            count++;
        })
        project.save(function(err, project){
            if (err) console.log(err);
            res.write(JSON.stringify(resData));
            res.end();
        });
    });
};

var getStatuses = function(res, projectId){
    var options = {};

    //create {value:10, status:complete, projectId:50f99d0d51a5d5450c000006}

    Project.findOne({_id: projectId}, function (err, project) {
        if(err)
            console.log(err);
        var statuses = [];
        var countByStatus = _.countBy(project.tasks, "status");
        for(var prop in countByStatus){
            statuses.push({value: countByStatus[prop], status :prop, projectId: projectId});
        }

        res.write(JSON.stringify( statuses));
        res.end();
    })
};

var completeTask = function(res,projectId, task){
    for (var i = task.tasks.length - 1; i >= 0; i--) {
        if(!task.tasks[i].complete)
            task.tasks[i].complete = true;
    };
    updateTask(res, task, projectId);
};

exports.getProjects = getProjects;
exports.addProject = addProject;
exports.removeProject = removeProject;
//exports.getProject = getProjectsById;
exports.updateProject = updateProject;
exports.getLowestProrityTask = getLowestProrityTask;
exports.addTask = addTask;
exports.getTasks = getTasks;
exports.updateTask = updateTask;
exports.removeTask = removeTask;
exports.getGroupFacets = getGroupFacets;
exports.getTaskCountPerProject  = getTaskCountPerProject;
exports.updateTasksPriority = updateTasksPriority;
exports.getStatuses = getStatuses;
exports.completeTask = completeTask;