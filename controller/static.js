var mime = require("mime");
var url = require("url");
var path = require("path");
var fs = require("fs");

var getFile = function(req, res){
	var uri = url.parse(req.url).pathname;
	var filename = path.join(process.cwd(), uri);
	
	//console.log("fetching file: " + uri);
	
	if(uri == null || uri == "")
		throw404(req, res);
	else if(uri== "/"){
		req.url += "view/index.html";
		getFile(req,res);
		return;
	}
		
	path.exists(filename, function(exists){
 		if(!exists){
 			throw404(req, res);		
 			return;
 		}
		

		fs.readFile(filename, "binary", function(err, file){
			//console.log('fetching file : ' + filename);
			//console.log( mime.lookup(filename));
			res.writeHead(200, {'Content-Type' : mime.lookup(filename) });
			res.write(file, "binary");
			res.end();
		});		
	});
};

var throw404 = function(req, res){
	req.url = "/view/error/page404.html";
	getFile(req, res);
	return;
};

exports.getFile = getFile;