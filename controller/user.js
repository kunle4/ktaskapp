var http = require('http');
//var app = require('express').createServer();
var UserSchema = require('./../model/user');
var dbName = 'ktask';
var collectionName = 'tasks';

var mongoose = require('mongoose');
//var connection = mongoose.connect('mongodb://localhost:27017/' + dbName);
var User = mongoose.model('User', UserSchema);

var sys = require("util");  
var test = require("assert");  

//sys.puts("Using mongodb://localhost:27017/");  

var addUser = function(req, res, callback){
	var userJson = {username:req.body.username, name: {first:req.body.firstname, last:req.body.lastname}, password:req.body.password, email:req.body.email};
	console.log("�dding user :" + JSON.stringify(userJson)); 
	var user = new User(userJson);
	user.save(function(){
		callback(user);
	});                      
};

var getUserById = function (id, req, res){
	User.find({_id:id}, function(err, user){
		res.write(JSON.stringify(user));
		res.end();
	});
};

var getUserByEmail = function (email, req, res){
	User.getUserByEmail(email, function(err, user){
		res.write(JSON.stringify(user));
		res.end();
	});
};

var authenticate = function (username, password, callback){
	User.authenticate(username,password, function(err,user){
		callback(user);
	});
};

exports.registerUser = addUser;
exports.getUserById = getUserById;
exports.getUserByEmail = getUserByEmail;
exports.authenticate = authenticate;

