/*
 * Project Model
**/
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var Project = module.exports = new Schema ({
    title		: { type: String, index: true }
  , description	: String
  , tags		: [String]
  , tasks       : [
        {
            title		: { type: String, index: true }
            , description	: String
            , tasks :[
                {
                    todo:String,
                    complete: {type:Boolean,default:false},
                    completionDate:{ type: Date}
                }
            ]
            , note		: String
                , tags		: [String]
                , taskDate 	: { type: Date, default:new Date() , index :true }
            ,	complete	: { type: Boolean }
            , completionDate:{type: Date }
            , priority  : { type: Number}
            , status    : {type: String, default: 'ongoing'}
            , group     : {type: String, default: ''}
            , activities  :[
                {
                    when: {type:Date, default: new Date(), index : true}
                    ,what : String
                    ,who  : Schema.ObjectId
                    ,context : {id: Schema.ObjectId, objectType : String}
                }
            ]
            , owner		: Schema.ObjectId
            , project		: Schema.ObjectId
        }
    ]
  , createdDate 	: { type: Date, default:new Date() , index :true }
  , activities  :[
      {
        when: {type:Date, default: new Date(), index : true}
        ,what : String
        ,who  : Schema.ObjectId
        ,context : {id: Schema.ObjectId, objectType : String}
      }
    ]
  , owner		: Schema.ObjectId
  , deleted     : {type: Boolean, default: false}
});

Project.statics.findProjectsByUser = function (userId, callback) {
  return this.find({ owner: userId , deleted: false}, callback);
};

Project.statics.removeProject = function (id, callback) {
    return this.update({ _id: id },{$set:{deleted: true}}, callback);
};

Project.statics.findProjectsByUserLean = function (userId,properties ,callback) {
    return this.find({ owner: userId, deleted: false },properties, callback);
};

Project.statics.findProjectsById = function (projectId, callback) {
    this.findOne({ _id: projectId }, callback);
};

Project.statics.getTasks = function(projectId, callback){
    this.findOne({_id: projectId}, function(err, project){
        callback(err,project.tasks || []);
    });
};

Project.statics.updateTask = function(projectId,task, callback){
    this.findOne({_id: projectId}, function(err, project){
        for(var i = 0; i < project.tasks.length; i++){
            if(project.tasks[i]._id){
                if(project.tasks[i]._id.toString() == task._id.toString()){
                    project.tasks[i] = task;
                    break;
                }
            }
        };
        project.save();
        callback(err,task);
    });
};

Project.statics.findTaskWithLowestPriority = function(projectId, callback){
    this.findOne({_id: projectId}, function(err, project){
         if(project.tasks.length > 0) {
             var ongoingTasks = [];
             for(var i = 0; i < project.tasks.length; i++){
                 if(!project.tasks[i].complete)
                    ongoingTasks.push(project.tasks[i]);
             }
            callback(err,ongoingTasks[ongoingTasks.length-1]);
         }
        callback(err,{});

    });
};
