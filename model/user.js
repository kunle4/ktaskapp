/*
 * User Model
**/
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var User = module.exports = new Schema ({
			username	:{type: String, index :{unique:true}, set: toLower}
			, password 	:{type: String}
			, email 	: {type: String}
			, name		:{
							  first: String
							, last: String
					}
			, created	:{type: Date,  default: Date.now(), index: true}
			, updated	:{type: Date}
});

function toLower (v) {
  return v.toLowerCase();
}

User.statics.authenticate = function (username, password, callback) {
	return this.find({ username: username, password:password }, function(err,users){
		if(users.length > 0)
			callback(err,users[0]);
		else
			callback(err,null);
	});
};

User.statics.getUserByEmail = function (username, callback) {
	return this.find({ username: username}, callback);
};