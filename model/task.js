/*
 * Task Model
**/
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var Task = module.exports = new Schema ({
    title		: { type: String, index: true }
  , description	: String
  , tasks :[{
      todo:String, 
      complete: {type:Boolean,default:false}, 
      completionDate:{
          type: Date          
        }
      }]
  , note		: String
  , tags		: [String]
  , taskDate 	: { type: Date, default:new Date() , index :true }
  ,	complete	: { type: Boolean }
  , completionDate:{type: Date }
  , priority  : { type: Number}
  , status    : {type: String, default: 'ongoing'}
  , group     : {type: String, default: ''}
  , activities  :[
      { 
        when: {type:Date, default: new Date(), index : true}
        ,what : String
        ,who  : Schema.ObjectId
        ,context : {id: Schema.ObjectId, objectType : String}
      }
    ]
  , owner		: Schema.ObjectId
  , project   : Schema.ObjectId
});

/**
 * Methods
 */

Task.statics.findByTitle = function (title, callback) {
	return this.find({ title: title }, callback);
};

Task.statics.findByProject = function (projectId, callback) {
    return this.find({ project: projectId }).sort('priority').execFind(callback);
};

Task.statics.findTasksByUser = function (filter, callback) {
	//return this.find({ owner: userId }, callback);
  //look for filters that are null and add the exists operator
  for(field in filter){
    if(filter[field] == null || filter[field] == 'null' || typeof filter[field] === 'undefined')
      filter[field] = { $exists: false};
  }
  if(filter != null && typeof filter !== "undefined" )
    return this.find(filter).sort('priority').execFind(callback);
  else  
    return this.find({ owner: userId }).sort('priority').execFind(callback);

};

Task.statics.findOngoingTasksByUser = function (userId, callback) {
  return this.find({ owner: userId, status:'ongoing' }).sort('priority').execFind(callback);
};

Task.statics.findLowestPrority = function(filter, callback){
   //look for filters that are null and add the exists operator
  for(field in filter){
    if(filter[field] == null || filter[field] == 'null' || typeof filter[field] === 'undefined')
      filter[field] = { $exists: false};
  }
  return this.findOne(filter).sort({priority: 'desc' }).execFind(callback);
};