'use strict';

/* Controllers */

angular.module('ktaskApp.controllers', []).
    controller('LoginCtrl', ['$scope','loginMessages',function($scope,loginMessagesService) {
        $scope.user = {username :null, password:null};
        $scope.redir = "/angular";
        $scope.messages = [];
        $scope.messageType = '';
        $scope.showMessages = 'false';
        loginMessagesService.getMessages(
            function(messages){
                if(messages != null && messages != undefined){
                    $scope.messages = messages;
                    $scope.messageType = messages && messages.length > 0 ? 'alert-error' : '';
                    $scope.showMessages = messages && messages.length > 0 ? true : false;
                }
            }
        );

    }])
    .controller('RegistrationCtrl', ['$scope',function($scope) {
        $scope.redir = "/angular";
    }]);