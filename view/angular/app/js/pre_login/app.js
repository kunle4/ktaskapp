'use strict';


// Declare app level module which depends on filters, and services
angular.module('ktaskApp', ['ktaskApp.controllers','kTaskApp.directives','ktaskApp.services']).
  config(['$routeProvider', function($routeProvider) {
    var templateRootPath = 'view/angular/app/partials/';

    $routeProvider.when('/login', {templateUrl: templateRootPath + 'login.html', controller: 'LoginCtrl'});
    $routeProvider.when('/registration', {templateUrl: templateRootPath + 'registration.html', controller: 'RegistrationCtrl'});
    $routeProvider.otherwise({redirectTo: '/login'});
  }]);
