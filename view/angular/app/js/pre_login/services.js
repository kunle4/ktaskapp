'use strict'

/* Services */
angular.module('ktaskApp.services', [])
    .factory('loginMessages', ['$http',function($http) {
        var obj = {};
        obj.getMessages = function (callback) {
            return $http.get('/login/messages', {}).
                success(
                function(data, status, headers, config) {
                    console.log(data);
                    if(data != null && data != "null" && data.splice) {
                        callback(data);
                        $http.post('/messages/clear');  //clear messages
                    }
                }).
                error(
                function(data, status, headers, config) {
                    console.log(data);
                });
        }
        return obj;
    }])
    .value('templateRootPath', '/view/angular/app/partials/');
