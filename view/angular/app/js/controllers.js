'use strict';

/* Controllers */

angular.module('ktaskApp.controllers', []).
    controller('projectCtrl', ['$scope','resource','projectUtils','$stateParams','$rootScope', '$dialog',
            'templateRootPath','Restangular',
        function($scope, resourceService,projectUtils, $stateParams,$rootScope,$dialog,templateRootPath,Restangular) {
        var Project = resourceService.getProjectResource();
        var Projects = Restangular.all('projects');
        var TasksPerProject = resourceService.getTasksPerProjectResource();
        console.log($stateParams);
        $rootScope.$broadcast('projectSwitch', null);

        $scope.projects = [];
        Projects.getList().then(function(projects){
            $scope.projects = projects;
            projectUtils.loadProjectsStats($scope.projects);
        });

        /*$scope.projects = Project.query(function(){
            projectUtils.loadProjectStats($scope.projects);
        });*/
        $scope.tasksPerProject = [];
        $scope.tasksPerProject = TasksPerProject.query();

        //setup project edit dialog
        $scope.projectEditDialogOptions = {
            backdrop: true,
            keyboard: true,
            backdropClick: true,
            dialogClass: 'project-modal modal',
            templateUrl: templateRootPath + 'project.html',
            controller: 'projectEditCtrl'
        };

        $scope.openEditDialog = function(project, index){
            $scope.projectEditDialogOptions.resolve = {context: function(){return {project:project, collection:$scope.projects, index:index};}}
            var d = $dialog.dialog($scope.projectEditDialogOptions);
            d.open();
        };

        $scope.openNewDialog = function(){
            $scope.projectEditDialogOptions.resolve = {context: function(){return {project:{},collection:$scope.projects};}}
            var d = $dialog.dialog($scope.projectEditDialogOptions);
            d.open();
        };

    }])
    .controller('projectEditCtrl', ['$scope', 'dialog', 'context','resource','projectUtils',
        function($scope, dialog, context, resourceService,projectUtils){
        var Project = resourceService.getProjectResource();
        var ProjectPersistService = resourceService.getProjectSaver();
        $scope.project = context.project;
        $scope.isNew = !context.project._id;

        $scope.close = function() { dialog.close() };

        $scope.save = function() {
            if($scope.project._id){
                $scope.project.put($scope.project._id).then(function(result){
                    console.log('After saving project:', result);
                    dialog.close();
                });
            }else{
                context.collection.post($scope.project).then(function(result) {
                    console.log('After saving project:', result);
                    if(result._id){
                        projectUtils.loadProjectStats(result);
                        context.collection.push(result);
                        dialog.close();
                    }
                });
            }
        };
        console.log('project dialog context:', context);
        $scope.remove = function() {
            $scope.project.remove().then(function(result){
                console.log('After removing project:', result);
                if(result){
                    //remove from collection
                     context.collection.splice(context.index,1);
                    dialog.close();
                }
            });
        };
    }])
    .controller('projectFacetCtrl', ['$scope','resource','$location','utils',
        function($scope, resourceService, $location,utils) {
        var TasksPerProject = resourceService.getTasksPerProjectResource();
        $scope.tasksPerProject = [];
        $scope.tasksPerProject = TasksPerProject.query();
        $scope.gotoProjects = function(hash){
            $location.hash(hash);
        }

        $scope.$on('taskRemoved', function(context) {
            var tasksPerProject = TasksPerProject.query({},function(){
                utils.updateCollection(tasksPerProject,$scope.tasksPerProject,'projectId');
            });
        });

        $scope.$on('taskUpdated', function(context) {
            var tasksPerProject = TasksPerProject.query({},function(){
                utils.updateCollection(tasksPerProject,$scope.tasksPerProject,'projectId');
            });
        });

    }])
    .controller('userCtrl', ['$scope','resource',function($scope,resourceService) {
        var CurrentUser = resourceService.getCurrentUserResource();
        $scope.user = {};
        $scope.user = CurrentUser.get();
    }])
    .controller('taskCtrl', ['$scope','resource','$stateParams','taskUtils','$location','$state','$rootScope',
        'Restangular', 'templateRootPath','$dialog','utils',
        function($scope,resourceService, $stateParams, taskUtils, $location, $state, $rootScope,Restangular,
                 templateRootPath, $dialog,utils) {
            console.log('resolved tasks controller...')
            var Task = resourceService.getTaskResource(),
                Status = resourceService.getProjectTaskStatuses(),
                PriorityService = resourceService.PriorityService(),
                projectId = $stateParams.id,
                criteria = $location.search();
            criteria.id = $stateParams.id;

            console.log(criteria);
            $scope.tasks = [];
            $scope.filteredtasks = [];
            $scope.statuses = [];

            $rootScope.$broadcast('projectSwitch', projectId, criteria);

            $scope.loadTasks = function(){
                console.log('loading tasks...');
                /*$scope.tasks = Task.query(criteria, function(){
                    taskUtils.loadTaskCompletionPercentage($scope.tasks);
                    $scope.filteredtasks = $scope.tasks;
                    setActiveStatus();
                });*/
                Restangular.one('projects',projectId).getList('tasks',{status: criteria.status}).then(function(tasks){
                    console.log('tasks from server:',tasks);
                    console.log(arguments);
                    $scope.tasks = tasks;
                    taskUtils.loadTasksCompletionPercentage($scope.tasks);
                    $scope.filteredtasks = $scope.tasks;
                    setActiveStatus();
                });
            };

            $scope.switchStatus = function(status){
                criteria.status = status;
                $scope.loadTasks();
                $rootScope.$broadcast('statusSwitch', status);
            };

            var loadStatuses = function(){
                $scope.statuses = Status.query({id:projectId}, function(){
                    setActiveStatus();
                });
            }

            $scope.remove = function(task, index) {
                task.remove().then(function(result){
                    console.log('After removing task:', result);
                    if(result){
                        $rootScope.$broadcast('taskRemoved', null);
                        //remove from collection
                        $scope.tasks.splice(index,1);
                    }
                });
            };

            var setActiveStatus = function(){
                _.each($scope.statuses, function(s){
                    s.isActive = false;
                });
                var statusInstance = _.findWhere($scope.statuses, {status:criteria.status })
                 if(statusInstance)
                     statusInstance.isActive = true;
            };


            var filterTasksByGroup = function(selectedGroups){
                if(selectedGroups.length == 0)
                    $scope.filteredtasks = $scope.tasks;
                else{
                    $scope.filteredtasks = _.filter($scope.tasks, function(task){
                        return _.contains(selectedGroups, task.group);
                    })
                }
            };

            var prioritizeTasks = function(ids){
                PriorityService.save(projectId,ids,function(data){
                   // console.log('back from the update priorities',data);
                });

            };

            //////Sortable options (drag and drop)
            $scope.sortableOptions = {
                handle: ".drag-handle",
                update: function(event, ui){
                    console.log('in the update sortable');
                },
                stop: function(event, obj){
                    /*console.log('in the stop of sortable'); */
                    console.log('arguments in stop handler: ', arguments);
                    console.log('arguments obj.item.context: ', obj.item.context);
                    console.log('arguments obj.item: ', obj.item);

                    if ($(this).hasClass('ignore-update')){
                        $(this).sortable('cancel');
                        $(this).removeClass('ignore-update');
                    } else{
                        console.log('going to prioritize');
                        var ids = $( this ).sortable('toArray');
                        console.log(ids);
                        prioritizeTasks(ids);
                    }
                },
                connectWith: "#status-container li"
            };
            //////Subscriptions
            $scope.$on('groupSelected', function(context,selectedGroups) {
                filterTasksByGroup(selectedGroups);
            });

            $scope.$on('taskRemoved', function(context) {
                var statuses = Status.query({id:projectId}, function(){
                    utils.updateCollection(statuses,$scope.statuses,'status');
                    setActiveStatus();
                });
            });

            $scope.$on('taskUpdated', function(context, taskInfo) {
                //determine if the task is complete, if so remove
                $scope.loadTasks();
                var statuses = Status.query({id:projectId}, function(){
                    utils.updateCollection(statuses,$scope.statuses,'status');
                    setActiveStatus();
                });
            });

            //setup task edit dialog
            $scope.taskEditDialogOptions = {
                backdrop: true,
                keyboard: true,
                backdropClick: true,
                dialogClass: 'custom-modal modal',
                templateUrl: templateRootPath + 'task.html',
                controller: 'taskEditCtrl'
            };

            $scope.openEditDialog = function(task, index){
                $scope.taskEditDialogOptions.resolve = {context: function(){return {task:task, collection:$scope.tasks, index:index};}}
                var d = $dialog.dialog($scope.taskEditDialogOptions);
                d.open();
            };

            $scope.openNewDialog = function(){
                $scope.taskEditDialogOptions.resolve = {context: function(){return {task:{},collection:$scope.tasks};}}
                var d = $dialog.dialog($scope.taskEditDialogOptions);
                d.open();
            };
            $scope.loadTasks();
            loadStatuses();
    }])
    .controller('taskEditCtrl', ['$scope', 'dialog', 'context','resource','taskUtils', '$rootScope','utils',
    function($scope, dialog, context, resourceService,taskUtils,$rootScope,utils){
        var Project = resourceService.getProjectResource();
        var ProjectPersistService = resourceService.getProjectSaver();
        $scope.task = context.task;
        $scope.isNew = !context.task._id;

        if($scope.isNew)
            $scope.task.tasks = [];

        console.log('task dialog context:', context);

        $scope.close = function() { dialog.close() };

        $scope.save = function() {
            if($scope.task._id){
                $scope.task.put($scope.task._id).then(function(result){
                    $rootScope.$broadcast('taskUpdated', {index: context.index, task: $scope.task});
                    /*taskUtils.loadTaskCompletionPercentage($scope.task);
                    console.log('After saving task:', result); */
                    dialog.close();
                });
            }else{
                context.collection.post($scope.task).then(function(result) {
                    $rootScope.$broadcast('taskUpdated', null);
                    console.log('After saving task:', result);
                    if(result.task._id){
                        taskUtils.loadTaskCompletionPercentage(result.task);
                        context.collection.unshift(result.task);
                        dialog.close();
                    }
                });
            }
        };
        $scope.onChange = function(todo,index){
            //if checked then place on top of all checked
            //if unchecked then place on top
            var tasks = $scope.task.tasks;
            console.log('todo',todo);
            if(todo.complete){
                var indexToInsertAt = null;

                for(var i = 0; i < tasks.length; i++){
                    var t = tasks[i];
                    if(t.complete && t.$$hashKey != todo.$$hashKey){
                        indexToInsertAt = i;
                        break;
                    }
                }

                if(indexToInsertAt == null){
                    tasks.splice(index,1); //remove
                    tasks.push(todo);//insert at end
                }
                else if(index != indexToInsertAt){
                    tasks.splice(index,1); //remove
                    tasks.splice(indexToInsertAt == 0 ? indexToInsertAt : (indexToInsertAt -1),0, todo);//insert at this index
                }

            }else{
                tasks.splice(index,1); //remove
                tasks.unshift(todo); //place on top
            }
        };

        $scope.removeTodo = function(index){
            $scope.task.tasks.splice(index,1);
        };

        $scope.addNewTodo = function(event){
            console.log(arguments);
            var value = event.currentTarget.value;
            var todo = {
                complete: false,
                todo: ''
            };
            if ( event.which !== 13 || !value.trim() ) {
                return;
            }
            event.currentTarget.value = '';
            todo.todo = value;
            $scope.task.tasks.unshift(todo);  //place on top
        };

        $scope.remove = function() {
            $scope.task.remove().then(function(result){
                console.log('After removing task:', result);
                if(result){
                    $rootScope.$broadcast('taskRemoved', null);
                    //remove from collection
                    context.collection.splice(context.index,1);
                    dialog.close();
                }
            });
        };

        $scope.sortableOptions = {
            handle: ".drag-handle-small",
            stop: function(event, obj){
                /*console.log('in the stop of sortable'); */
                console.log('arguments in stop handler: ', arguments);
                console.log('arguments obj.item.context: ', obj.item.context);
                console.log('arguments obj.item: ', obj.item);
                var ids = $( this ).sortable('toArray');
                console.log(ids);
                var todos = $scope.task.tasks;
                var newTodos = [];
                for(var i = 0; i < ids.length; i++){
                    var index = ids[i];
                    newTodos.push(todos[index]);
                }
                $scope.task.tasks = newTodos;
            }
        };
    }])
    .controller('taskFilterCtrl', ['$scope','resource','$stateParams','$location','$rootScope','utils',
        function($scope,resourceService, $stateParams, $location,$rootScope,utils) {
        $scope.groups = [];
        var Group = resourceService.getProjectTaskGroupings(),
            criteria = $location.search(),

            loadGroupsFilters = function(){
                $scope.groups = Group.query(criteria);
            },

            updateGroupFilters = function(){
                var groups = Group.query(criteria, function(){
                    utils.updateCollection(groups,$scope.groups,'group');
                });
            }

        $scope.groupChange = function(){
            var selectedGroups = _.pluck(_.where($scope.groups, {checked: true}),'group');
            //console.log('group checked event',selectedGroups);
            $rootScope.$broadcast('groupSelected', selectedGroups);
        };

        ////Subscriptions
        $scope.$on('projectSwitch', function(context,projectId, newCriteria) {
            if(!projectId){
                $scope.groups = [];
                return;
            }
            criteria= newCriteria;
            criteria.id = projectId;
            loadGroupsFilters();
        });

        $scope.$on('statusSwitch', function(context,status) {
            criteria.status = status;
            loadGroupsFilters();
        });

        $scope.$on('taskRemoved', function(context) {
            updateGroupFilters();
        });

        $scope.$on('taskUpdated', function(context) {
            updateGroupFilters();
        });


    }]);