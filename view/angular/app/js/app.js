'use strict';


// Declare app level module which depends on filters, and services
angular.module('ktaskApp', ['ktaskApp.services','ktaskApp.controllers',
                'ngResource','kTaskApp.directives','ui.compat','ui.bootstrap','ui.sortable','restangular','tags-input']).
  config(['$routeProvider','$stateProvider','$urlRouterProvider', function($routeProvider,$stateProvider,$urlRouterProvider) {
    var templateRootPath = 'view/angular/app/partials/';

    $routeProvider.when('', {redirectTo:'/projects'});
    $routeProvider.when('/', {redirectTo:'/projects'});
    $routeProvider.when('/angular', {redirectTo:'/projects'});
    //.when('/', {redirectTo:'/projects'});
    /*.when('/projects/:id/tasks', {templateUrl: templateRootPath +'tasks.html', controller: 'tasksCtrl'})
    .when('/projects', {templateUrl: templateRootPath +'main.html', controller: 'projectCtrl'})
    .otherwise({redirectTo: '/'});  */

    /*  $stateProvider
        .state('root', {
            url: '',
            abstract: true,
            views:
            {
                '':{
                    templateUrl: templateRootPath + 'topNav.html',
                    controller: 'userCtrl'
                }
            }
        })
       .state('root.projects', {
            url: '/projects',
            views:
            {
                'projects@root':
                {
                    templateUrl: templateRootPath + 'projects.html',
                    controller: 'projectCtrl'
                }
            }

        })  */
    $stateProvider
        .state('projects', {
            url: '/projects',
            templateUrl: templateRootPath +'projects.html',
            controller: 'projectCtrl'

        })
        .state('tasks', {
            url: '/projects/:id/tasks',
            templateUrl: templateRootPath +'tasks.html',
            controller: 'taskCtrl'

        });
  }])
    .config(function(RestangularProvider) {
        RestangularProvider.setExtraFields(['title']);
        RestangularProvider.setRestangularFields({id: "_id" });
    })
    .run(['$rootScope', '$state', '$stateParams',
        function ($rootScope,   $state,   $stateParams) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }]);
