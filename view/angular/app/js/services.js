'use strict'

/* Services */
angular.module('ktaskApp.services', [])
    .factory('resource', ['$resource','$http',function($resource, $http) {
        var obj = {};

        obj.getProjectResource = function () {
            return $resource('/projects/:id');
        };
        obj.getProjectSaver = function(){
            return function(project, success){
                var url = '/projects/';
                if(project._id)
                    url += project._id
                return $http.put(url,project).
                    success(
                        function(data, status, headers, config) {
                           console.log(data);
                            if(success)
                                success();
                        }).
                        error(function(data, status, headers, config) {
                            console.error(data);
                        });
            };
        };
        obj.getTasksPerProjectResource = function () {
            return $resource('/projects/count_per_project');
        };
        obj.getCurrentUserResource = function () {
            return $resource('/user/current');
        };
        obj.getTaskResource = function () {
            return $resource('/projects/:id/tasks/');
        };
        obj.getProjectTaskStatuses = function () {
            return $resource('/projects/:id/tasks/statuses');
        };
        obj.getProjectTaskGroupings = function(){
            return $resource('/projects/:id/count_per_group');
        };
        obj.PriorityService = function(){
            return {
                save: function(projectId, ids, success){
                    var url = '/projects/' + projectId + '/prioritize/';
                    jQuery.ajax({
                        type: "POST",
                        url: url,
                        contentType : 'application/json',
                        data: JSON.stringify(ids)
                    }).done(function( response ) {
                            if(success)
                                success(response);
                    });
                }
            }
        };
        return obj;
    }])
    .factory('projectUtils', [function(){
        var obj = {};
        obj.loadProjectStats = function(project){
            project.stats = {};
            project.stats.tasksCount = project.tasks.length;
            project.stats.tasksCompleted = _.filter(project.tasks, function(task){ return task.complete;}).length;
            project.stats.percentCompleted = project.stats.tasksCount== 0 ? 0 : ((project.stats.tasksCompleted/project.stats.tasksCount) * 100).toFixed(0);
        };
        obj.loadProjectsStats = function(projects){
            _.each(projects, function(project){
                obj.loadProjectStats(project);
            });
        };
        return obj;
    }])
    .factory('taskUtils', [function(){
        var obj = {};
        obj.loadTaskCompletionPercentage = function(task){
            task.completion = 0;
            var total = 0;
            if(task.tasks)
                total = task.tasks.length || 0;
            var completionCount = 0,
                completion = 0;
            if(total > 0) {
                _.each(task.tasks, function(todo){
                    if(todo.complete)
                        completionCount++;
                })
                completion = Math.round(completionCount/total * 100);
            }
            task.completion = completion;
        };
        obj.loadTasksCompletionPercentage = function(tasks){
            _.each(tasks, function(task){
                obj.loadTaskCompletionPercentage(task);
            });
        }
        return obj;
    }])
    .factory('utils', [function(){
        var obj = {
            updateCollection : function(newCollection, originalCollection, keyField){
                var newItems = [],
                    indicesToRemove = [];

                _.each(newCollection, function(item){
                    var itemToUpdate = _.find(originalCollection, function(r){
                        return r[keyField] == item[keyField];
                    });
                    if(itemToUpdate){
                        console.log('going to update:', itemToUpdate);
                        for(var prop in item){
                            itemToUpdate[prop] = item[prop];
                        }
                    }
                    else{
                        newItems.push(item);
                    }
                });
                //find old items to remove
                for(var i = 0; i < originalCollection.length; i++){
                    var foundItem = _.find(newCollection, function(r){
                        return r[keyField] == originalCollection[i][keyField];
                    })
                    if(typeof foundItem === "undefined"){
                        indicesToRemove.push(i);
                    }
                }

                //remove items that are not in new collection
                _.each(indicesToRemove, function(index){
                    originalCollection.splice(index,1);
                });

                //add new items that are not in original collection
                _.each(newItems, function(item){
                    originalCollection.push(item);
                });
            }
        };
        return obj;
    }])
    .value('templateRootPath', '/view/angular/app/partials/');


