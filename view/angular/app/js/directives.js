'use strict';

/* Directives */


angular.module('kTaskApp.directives', [])
    .directive('ngTemplate', function($http, $compile, templateRootPath) {
        return {
            restrict: 'A',
            transclude: true,
            replace: true,
            link: function(scope, element, attrs)
            {
                var templatePath = attrs.ngTemplate;
                if(templateRootPath == undefined) {
                    throw 'templateRootPath "value" is not defined'
                }

                if(templatePath == undefined || templatePath.length < 1){
                    console.error('Template name needed!');
                    return;
                }
                templatePath =  templateRootPath + templatePath + '.html';

                $http.get(templatePath).
                    success(
                    function(data, status, headers, config) {
                        $compile(element.html(data).contents())(scope.$new());
                    }).
                    error(function(data, status, headers, config) {
                        console.log(data);
                    });
            }
        };
    })
    .directive('teaser', function($compile) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs)
            {
                var teaserFunc = function(text, limit) {
                    if(text.length && text.length > limit)
                        return text.substring(0,limit) + ' ...';
                    return text;
                };

               scope.$watch(attrs.teaser, function(text){
                    element.html(teaserFunc(text, attrs.limit)).attr('title',text);
                }, true);


            }
        };
    })
    .directive('numCap', function($compile) {
        return {
            restrict: 'A',
           /* scope:{
                limit: "&",
                numCap: "&"
            },*/
            link: function(scope, element, attrs)
            {
                var numCapFunc = function(num, limit) {
                    if(num > limit)
                        return limit +"+";
                    return num;
                };

                scope.$watch(attrs.numCap, function(num){
                    element.html(numCapFunc(num, attrs.limit)).attr('title',num);
                }, true);
            }
        };
    })
    .directive('teaserRepeat', function(){
        return {
            transclude : 'element',
            compile : function(element, attr, linker){
                return function($scope, $element, $attr){
                    var loopString = $attr.teaserRepeat,
                        teaserLimit = parseInt($attr.teaserLimit) - 1 || 5,
                        teaserTitle = $attr.teaserTitle || "...",
                        match = loopString.match(/^\s*(.+)\s+in\s+(.*?)\s*(\s+track\s+by\s+(.+)\s*)?$/),
                        indexString = match[1],
                        collectionString = match[2],
                        parent = $element.parent(),
                        elements = [];


                    // $watchCollection is called everytime the collection is modified
                    $scope.$watch(collectionString, function(collection){
                        var i, block, childScope;

                        // check if elements have already been rendered
                        if(elements.length > 0){
                            // if so remove them from DOM, and destroy their scope
                            for (i = 0; i < elements.length; i++) {
                                elements[i].el.remove();
                                elements[i].scope.$destroy();
                            };
                            elements = [];
                        }
                        var getPrimaryText = function(references, theScope){
                            var child = theScope;
                            for(var i = 0; i < references.length; i++){
                                if(typeof child !== "undefined")
                                    child = child[references[i]];
                            }
                            return child;
                        };
                        var tooltipHtml = '',
                            isAtLimit = false;

                        for (i = 0; i < collection.length; i++) {
                            //we want to break of the loop
                            if(teaserLimit == (i -1) )
                                isAtLimit = true;
                            // create a new scope for every element in the collection.
                            childScope = $scope.$new();
                            // pass the current element of the collection into that scope

                             childScope[indexString] = collection[i];

                            linker(childScope, function(clone){
                                if(isAtLimit){
                                    var teaserPrimaryElement = parent.children().children().attr('teaser-primary');
                                    if(teaserPrimaryElement) {
                                        tooltipHtml +=  "<li style='list-style: none;font-size: 10px;'>"+getPrimaryText(teaserPrimaryElement.split('.'),childScope) + "</li>";
                                    }
                                }

                                // clone the transcluded element, passing in the new scope.
                                if(!isAtLimit){
                                    parent.append(clone); // add to DOM
                                    block = {};
                                    block.el = clone;
                                    block.scope = childScope;
                                    elements.push(block);
                                }
                                if(i == (collection.length - 1) && isAtLimit) {
                                    var $more = $("<div style='margin-bottom: 5px;margin-top:-10px;font-weight:bold;'>...</div>");
                                    parent.append($more);
                                    jQuery($more).popover({title:teaserTitle,content:tooltipHtml, html :true, placement:'left', trigger:'hover'});
                                }
                            });

                        };
                    });
                }
            }
        }
    });
