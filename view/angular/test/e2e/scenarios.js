'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('ktask App', function() {

  beforeEach(function() {
    browser().navigateTo('/angular');
  });


  it('should automatically redirect to /login when location hash/fragment is empty', function() {
    expect(browser().location().url()).toBe("/login");
  });


  describe('login', function() {

    beforeEach(function() {
      browser().navigateTo('#/login');
    });

    it('should render login when user navigates to #/login', function() {
      expect(element('[ng-view] form h2').text()).
        toMatch(/Login/);
    });

  });

});
