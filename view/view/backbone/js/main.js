var taskManager = taskManager === undefined ? {} : taskManager;

taskManager.currentTaskStatusView = "ongoing";
taskManager.taskView = taskManager.taskView || {};
taskManager.projectView = taskManager.projectView || {};
taskManager.collection = taskManager.collection || {};
taskManager.projectCollection = taskManager.projectCollection || {};
taskCollection = taskManager.collection;
projectCollection = taskManager.projectCollection;
taskManager.user = taskManager.user || {};
taskManager.userView = taskManager.userView || {};
taskManager.projectFacetTemplate = taskManager.projectFacetTemplate || {};
taskManager.switchToTasks = taskManager.switchToTasks || {};
taskManager.isTemplatesReady = false;
taskManager.currentProject = taskManager.currentProject || null;
taskManager.currentGroup = taskManager.currentGroup || null;
taskManager.isProjectView = taskManager.isProjectView  || true;
taskManager.isTaskView = taskManager.isTaskView || !taskManager.isProjectView; 
taskManager.taskGroupArray = [];
taskManager.projectTaskCount = {};
taskManager.projectStatuses = {};
taskManager.taskTemplateSelector = {
	tiled: '#task-template',
	stacked: '#task-template-stacked'
};
taskManager.taskEditMode = {
	tiled: {
		templateSelector: '#task-template-edit'
	},
	stacked: {
		templateSelector: '#task-template-stacked-edit'
	}
};
taskManager.currentTaskViewLayout = 'tiled';
taskManager.groupsByProject = {};

taskManager.loadTemplates = function(callbacks){
	$.get('/view/backbone/js/template/task.template.htm', function(data) { 
    	$('body').append($(data).filter('script')); //attach the template node to the body so its available
	});
	$.get('/view/backbone/js/template/project.template.htm', function(data) { 
    	$('body').append($(data).filter('script')); //attach the template node to the body so its available
    	console.log("templates are done loading");
    	utils.executeCallbacks(callbacks);
	});
};

taskManager.addNewSubTask = function(){
	var newView = new SubTaskView({model: new SubTask()});
	$('#subtask-container').prepend(newView.render().el);
};

taskManager.renderStatusView =function(){
	var options ={};
	options.filter= {project: taskManager.currentProject};

	var statusCollection = new Statuses(null,options);
	statusCollection.fetch(
		{
			success: function(collection, response){		
				console.log('back from the server after fetching statuses');
				var sView = new StatusCollectionView({model:collection});
				sView.render();
				$('#'+taskManager.currentTaskStatusView).addClass('active');

				 $('.status,.project-facet').droppable({
		            accept: "#items-container > li",
		            activeClass: "drop-highlight",
		            hoverClass: "drop-hover",
		            tolerance: 'pointer',
		            drop: function( event, ui ) {
		                console.log( 'should be removing the dragged item');		                
		                if($(this).hasClass('status'))
		                	taskManager.statusDropHandler(this, ui, event);
		                else	
		                	taskManager.sideBarDropHandler(this, ui, event);		                
		            }
		        });
			},
			error: function(){console.log('error');}			
		}	
	);	
};

taskManager.renderGroupView =function(){
	var project = taskManager.currentProject;
	var status = taskManager.currentTaskStatusView;

    $.getJSON('/projects/'+project+'/count_per_group?status='+status, function(data) {
    	var projectCounts = {};
    	taskManager.groupFacetTemplate = $('#group-facet-item-template').html();
    	var htemplate = Handlebars.compile(taskManager.groupFacetTemplate);

        var groups = taskManager.groupsByProject[taskManager.currentProject] || [];
    	var facetContainer = $('#group-facet-container');
    	facetContainer.html("");
    	for(i in data){

    		if(data[i].group == "" || data[i].group == null){
    			data[i].group = "undefined";
    		}
    		else{
    			taskManager.taskGroupArray[i] = data[i].group;
    		}
            var group = _.findWhere(groups, {group: data[i].group});
            if(!group){
                group = data[i]
                group.selected = false;
                groups.push(group);
            }
            else{
                group.value = data[i].value;
            }
    		facetContainer.append(htemplate(group));
    	}
        taskManager.groupsByProject[taskManager.currentProject] = groups;
    	if(taskManager.currentGroup != null){
    		$('[data-val-name="'+taskManager.currentGroup+'"]').addClass('active');
    		//add the clear filter button
    		var clearBtn = '<li id="clear-group-filter" title="clear filter"><a><i class="icon icon-remove"></i></a></li>';
    		facetContainer.append(clearBtn);
    	}				
	});	
};

taskManager.makeTaskContainerSortable = function(){
	$( "#items-container" ).sortable({
		handle: ".drag-handle",
		update: function(event, ui){
			//check if out or in
			console.log('in the update sortable');
		},
		stop: function(){
			console.log('in the stop of sortable');
			//console.log(this);
			if ($(this).hasClass('ignore-update')){
	            $(this).sortable('cancel');
	            $(this).removeClass('ignore-update');
        	} else{
        		console.log('going to prioritize');
        		console.log($( "#items-container" ).sortable('toArray'));
        		taskManager.updateTasksPriority($( "#items-container" ).sortable('toArray'));
        	}  
		},
		connectWith: "#status-container li"
	});
};

taskManager.updateTasksPriority = function(data){
	$.ajax({
	    type: "POST",
	    url: "/projects/"+taskManager.currentProject +"/prioritize/",
	    contentType : 'application/json',
	    data: JSON.stringify(data)
	}).done(function( response ) {
	    //console.log(response);
	    for (var i = 0; i < data.length; i++) {
			taskCollection.get(data[i]).set({priority:i+1});
		}
		taskCollection.fetch({
			success: function(collection, response){		
				console.log('back from the server after fetching tasks');			
				taskCollection.sort();
				taskManager.taskView.render();
			},
			error: function(){console.log('error');}	
		});
	});		
};

taskManager.completeTask = function(data){
	$.ajax({
	    type: "POST",
	    url: "/projects/"+taskManager.currentProject+"/tasks/complete",
	    contentType : 'application/json',
	    data: JSON.stringify(data)
	}).done(function( response ) {
	    taskCollection.fetch({success:taskManager.onSuccessTasksFetch, error:taskManager.onErrorTasksFetch});
	});		
};

taskManager.changeTasksView = function(filter, successCallback, errorCallback){
	taskManager.currentProject = filter.project;
	taskCollection.filter = filter;	
	taskCollection.fetch({success:successCallback, error:errorCallback});
};

taskManager.onSuccessTasksFetch =function(collection, response){
	console.log('Fetch tasks from server');			
	taskCollection.sort();
	taskManager.taskView.render();
};

taskManager.onErrorTasksFetch =function(error){
	console.log('Fetch tasks error from server');			
	console.log(error);
};

taskManager.loadTasks = function(options,callback){	
	taskCollection = new Tasks(null, {filter:options});
    $('.task-view-choices').removeClass('hide');
    $('.task-view-choices').show();
    taskCollection.fetch({
		success: function(collection, response){		
			console.log('Fetch tasks from server from loadTasks');			
			taskCollection.sort();
			taskManager.taskView = new TaskCollectionView({
				model:taskCollection, 
				templateSelector: taskManager.getTaskTemplateSelector(),
				renderEditMode : taskManager.getTaskEditMode()
			});
    		taskManager.taskView.render();
    		taskManager.setViewFlags(true);
    		taskManager.showHideViewComponents();
    		if(typeof callback === "function")
    			callback();
		},
		error: function(){console.log('error');}	
	});
};

taskManager.loadProjects = function(callback){	
	taskManager.projectCollection = new Projects();

	taskManager.projectCollection.fetch({
		success: function(collection, response){		
			console.log('Fetch projects from server');	
			//console.log(collection);		
			taskManager.projectView = new ProjectCollectionView({model:collection});
    		taskManager.projectView.render();
    		taskManager.setViewFlags(false);
    		taskManager.showHideViewComponents();
		},
		error: function(){console.log('error');}	
	});
};

taskManager.loadCurrentUser = function(){
	$.getJSON('/user/current', function(data) { 
    	taskManager.user = new User(data);
    	taskManager.userView = new UserView({model:taskManager.user});
    	taskManager.userView.render();
	});		
};

taskManager.getTaskCountByProject = function(){
	var deferred = Q.defer();
	$.getJSON('/projects/count_per_project', function(data) {
    	taskManager.projectTaskCount = {};
    	
    	for(var i = 0; i < data.length; i++){
    		if(data[i].projectId != null ){
    			taskManager.projectTaskCount[data[i].projectId]={
    					count : data[i].value
    					,projectId : data[i].projectId
    				};
    		}
    		else if(data[i].projectId == null ){
    			taskManager.projectTaskCount["undefined"]={
    					count : data[i].value
    					,projectId : "undefined"
    				};
    		}
    	}
    	deferred.resolve(data);
    });
	return deferred.promise;
};

taskManager.loadSideBarProjectLinks = function(){

	//$.getJSON('/tasks/count_per_project', 
	taskManager.getTaskCountByProject().then(function(data) { 
    	taskManager.projectTaskCount = {};
    	
    	for(var i = 0; i < data.length; i++){
            taskManager.projectTaskCount[data[i].projectId]={
                    name: data[i].name
                    ,count : data[i].value
                    ,projectId : data[i].projectId
                };
    	}

    	taskManager.projectFacetTemplate = $('#project-facet-item-template').html();
    	var htemplate = Handlebars.compile(taskManager.projectFacetTemplate);
    	var facetContainer = $('#project-facet-container');
    	facetContainer.html("");
    	for(id in taskManager.projectTaskCount){
    		//console.log(taskManager.projectTaskCount[id]);
    		facetContainer.append(htemplate(taskManager.projectTaskCount[id]));
    	}	
    	$('[data-project-id="'+taskManager.currentProject+'"]').addClass('active');
	});
};

taskManager.switchToTasks = function(event){
	console.log("clicked swtich to tasks view");
	var projectId = $(event.currentTarget.outerHTML).attr('data-project-id');
	var options	= {status: "ongoing"};
	taskManager.currentTaskStatusView = "ongoing";
	taskManager.currentProject = projectId;
	options.project = (typeof projectId !== 'undefined' && projectId != 'undefined') ? projectId : null;

	//if taskCollection is a backbone collection instance then run changeview
	if(typeof taskCollection['fetch'] !== "undefined")
		taskManager.changeTasksView(options,taskManager.onSuccessTasksFetch, taskManager.onErrorTasksFetch);
	else
		taskManager.loadTasks(options, taskManager.makeTaskContainerSortable);
	return false;
};

taskManager.sideBarDropHandler = function(dropElement, dragContext, event){
	//console.log(dragContext.draggable.parent());
    //console.log(dropElement);
    dragContext.draggable.parent().addClass('ignore-update');

    var taskIds = [];
    var taskElements = dragContext.draggable;
    var projectId = $(dropElement).attr('data-project-id');
    for (var i = 0; i < taskElements.length; i++) {
    	taskIds[i] = $(taskElements[i]).attr('id');
    };
	taskManager.updateTasksProject(taskIds, projectId);
};

taskManager.statusDropHandler = function(dropElement, dragContext, event){
	console.log('dropped in the status bucket');	
	dragContext.draggable.parent().addClass('ignore-update');
	var taskElement = dragContext.draggable[0];
	if($(dropElement).attr('data-status') == 'complete'){
		var task = taskCollection.get(($(taskElement).attr('id')));
		console.log('the task', task.toJSON());
		if(task)
			taskManager.completeTask(task.toJSON());
	}

};

taskManager.updateTasksProject = function(taskIds, projectId){
	var tasks = [];
	for(i = 0; i < taskIds.length; i++){
		tasks[i] = (taskCollection.get(taskIds[i]).toJSON());
		tasks[i].project = projectId;
	}
	taskManager.updateTasks(tasks, [taskManager.loadSideBarProjectLinks]);
};

taskManager.updateTasks = function(tasks, callbacks){
	$.ajax({
	    type: "POST",
	    url: "/tasks/update_batch",
	    contentType : 'application/json',
	    data: JSON.stringify(tasks)
	}).done(function( response ) {
	    console.log('back from the server after updating tasks');
	    utils.executeCallbacks(callbacks);
		taskCollection.fetch({
			success: function(collection, response){		
				console.log('back from the server after fetching tasks');			
				taskCollection.sort();
				taskManager.taskView.render();
			},
			error: function(){console.log('error');}	
		});
	});		
};

taskManager.setViewFlags = function (isTaskView){
	taskManager.isTaskView = isTaskView;
	taskManager.isProjectView = !isTaskView;	
};

taskManager.showHideViewComponents = function(){
	if(taskManager.isTaskView){
		$('.status-nav').removeClass('hide-status');
		$('#new-project').hide();
	}
	else{
		$('.status-nav').addClass('hide-staus');
		$('#new-project').show();
	}
};

taskManager.updateStats = function(){
	taskManager.loadSideBarProjectLinks();
	taskManager.renderStatusView();
	taskManager.renderGroupView();
};

taskManager.getTaskTemplateSelector = function(){
	if(taskManager.currentTaskViewLayout.length < 1)
		return taskManager.taskTemplateSelector.tiled;
	return taskManager.taskTemplateSelector[taskManager.currentTaskViewLayout];
};

taskManager.getTaskEditMode = function(){
	if(taskManager.currentTaskViewLayout.length < 1)
		return taskManager.taskEditMode.tiled;
	return taskManager.taskEditMode[taskManager.currentTaskViewLayout];
};

taskManager.switchTaskViewLayout = function(format){
	taskManager.currentTaskViewLayout = format;
	taskManager.cleanupEditMode();
	taskManager.taskView.options.templateSelector = taskManager.getTaskTemplateSelector();
	taskManager.taskView.options.renderEditMode = taskManager.getTaskEditMode();
	taskManager.taskView.render();
};

taskManager.cleanupEditMode = function(){
	$('#temp-container').html('');
};

taskManager.taskEditMode.tiled.renderEdit = function(event, templateSelector){
	console.log('in render edit');
	//console.log($(event.currentTarget.outerHTML).attr("data-task-id"));
	var taskId = $(event.currentTarget.outerHTML).attr("data-task-id");
	var task = taskManager.taskView.model.get(taskId);
	
	var htemplate = Handlebars.compile($(templateSelector).html());
	var editEl = htemplate(task.toJSON());
	this.editMode = true;
	$('#temp-container').html(editEl);
	$('#div-edit-'+task.id).modal({});
	
	$("#subtask-container").sortable();
	$('.tags-input').tagsInput(
		{
			'defaultText':'Tag this task',
			'width':'97%',
			'placeholderColor' : '#CCC'

		}
	);
  	$( "#task-group" ).autocomplete({
  		source: taskManager.taskGroupArray
   	});
	$('#new-todo').on('keypress', taskManager.taskView, taskManager.taskView.addSubTaskOnEnter);
	$('[id^="save-"]').on('click',taskManager.taskView, taskManager.taskView.save);
	$('[id^="subtask-status-"]').on('change', taskManager.taskView.prioritizeSubtask);
};

taskManager.taskEditMode.stacked.renderEdit = function(event, templateSelector){
	console.log('in render edit');
	//console.log($(event.currentTarget.outerHTML).attr("data-task-id"));
	var taskId = $(event.currentTarget.outerHTML).attr("data-task-id");
	var task = taskManager.taskView.model.get(taskId);
	
	var htemplate = Handlebars.compile($(templateSelector).html());
	var editEl = htemplate(task.toJSON());
	this.editMode = true;
	$('#temp-container').html(editEl);
	//$('#div-edit-'+task.id).modal({});
	
	$("#subtask-container").sortable();
	$('.tags-input').tagsInput(
		{
			'defaultText':'Tag this task',
			'width':'97%',
			'placeholderColor' : '#CCC'

		}
	);
  	$( "#task-group" ).autocomplete({
  		source: taskManager.taskGroupArray
   	});
	$('#new-todo').on('keypress', taskManager.taskView, taskManager.taskView.addSubTaskOnEnter);
	$('[id^="save-"]').on('click',taskManager.taskView, taskManager.taskView.save);
	$('[id^="subtask-status-"]').on('change', taskManager.taskView.prioritizeSubtask);
};

