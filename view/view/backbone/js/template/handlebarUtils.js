Handlebars.registerHelper('teaser', function(options, limit) {
	var text = options;
	if(text.length > limit)
        return text.substring(0,limit) + ' ...';
    return text;
});

Handlebars.registerHelper('numberCap', function(options, limit) {
	var num = options;
	if(num > limit)
        return limit - 1 +"+";
    return num;
});

Handlebars.registerHelper('shortList', function (items,limit,options){
	var buffer = "";
	limit = (items.length <= limit) ? items.length : limit;

	for(var i = 0; i < limit; i++){
		items[i].itemSize = '';
		buffer += options.fn(this.tasks[i], items[i]);
		
	}
	if(limit < items.length)
		buffer += "<span>...</span>";
	//console.log(buffer);
	return buffer;
});

Handlebars.registerHelper('percentageCompletion', function(items){
	//console.log(options);
	var completionCount = 0;
	var completion = 0;

	if(items.length > 0){
		for (var i = items.length - 1; i >= 0; i--) {
			if(items[i].complete)
				completionCount++;
		};
		completion = Math.round(completionCount/items.length * 100);
	}
	return completion + '%';
});