var utils = utils || {
	executeCallbacks : function(callbacks){
		if(typeof callbacks !== "undefined" && callbacks != null && callbacks.length > 0){
    		for(index in callbacks){
    			if(typeof callbacks[index] === "function")
    				callbacks[index]();
    		}
    	}
	}
}