window.Project = Backbone.Model.extend({
    initialize: function(){
    },
    urlRoot: '/projects/',
    idAttribute: "_id",
    defaults:{
        _id:null,
    	title: "",
    	description :"",
    	createdDate: new Date(),
    	tags : [],
        activities  : []
    }
});

window.Projects = Backbone.Collection.extend({
    initialize: function(models, options){
    },
     model : Project,
     urlRoot: '/projects/',
     url: '/projects/'
});