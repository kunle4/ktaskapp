window.Task = Backbone.Model.extend({
    initialize: function(){
        /*this.on("change",function(model){
        
        });*/
    },
    urlRoot: '/tasks/',
    idAttribute: "_id",
    defaults:{
        _id:null,
    	title: "",
    	description :"",
    	taskDate: new Date(),
        tasks: [],
    	tags : [],
        group: "",
        priority : 0,
        status: 'ongoing',
        completionDate : null
        //project: '50f99d0d51a5d5450c000006'
    },
    url: function(){
        return '/projects/'+this.attributes.project+'/tasks/' + (this.attributes._id || '');
    }
});

window.SubTask = Backbone.Model.extend({
    initialize: function(){
    },
    idAttribute: "_id",
    defaults:{
        complete:false,
        todo:""
    } 
});



window.Tasks = Backbone.Collection.extend({
    initialize: function(models, options){
        this.filter = options.filter;
    },
	 model : Task,
     urlRoot: '/tasks/',
     url: function(){
        if(this.filter === null || typeof this.filter === "undefined")
            return this.urlRoot;
        else{
            var qs = '?';
            var projectId = '';
            for(var prop in this.filter){
                if(prop == "project")
                    projectId = this.filter[prop];
                else
                    qs += prop + '='+this.filter[prop] + '&';
            }
            qs = qs.lastIndexOf('&') == qs.length -1 ? qs.substring(0,qs.lastIndexOf('&')) : qs; //chop off the last ampersand
            return '/projects/'+ projectId + this.urlRoot + qs;
        }
    },
     comparator : function(task) {
        //console.log('in the comparator');
        return -(task.get("priority"));
    }
});

window.SubTasks = Backbone.Collection.extend({
    initialize: function(){
    },
     model : SubTask,
     removeOne :function(item){

     }
});

window.User = Backbone.Model.extend({
    initialize : function(){
    }
});

//Status
window.Status = Backbone.Model.extend({
    initialize: function(){
    },
    idAttribute: "status"
});

window.Statuses = Backbone.Collection.extend({
    initialize: function(models, options){
        this.filter = options.filter;
    },
    model : Status,
    urlRoot: '/tasks/statuses',
    url: function(){
        if(this.filter === null || typeof this.filter === "undefined")
            return this.urlRoot;
        else{
            var qs = '?';
            var projectId = '';
            for(var prop in this.filter){
                if(prop == "project")
                    projectId = this.filter[prop];
                else
                qs += prop + '='+this.filter[prop] + '&';
            }
            qs = qs.lastIndexOf('&') == qs.length -1 ? qs.substring(0,qs.lastIndexOf('&')) : qs; //chop off the last ampersand
            //return this.urlRoot + qs;
            return '/projects/'+ projectId + this.urlRoot + qs;
        }
    }
    
});