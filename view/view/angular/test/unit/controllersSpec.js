'use strict';

/* jasmine specs for controllers go here */

describe('controllers', function(){
  beforeEach(module('ktaskApp.controllers'));

    var scope, loginController;
    beforeEach(inject(function($controller,$rootScope){
        scope = $rootScope.$new();
        loginController = $controller("LoginCtrl", {$scope:scope});
    }));

    it('username and password should be empty by default', inject(function() {
      expect(scope.user.username).toEqual(null);
      expect(scope.user.password).toEqual(null);
    }));

    it('user should scope needs to be updated with user entry when login executed', inject(function() {
        scope.user.username = 'test';
        scope.user.password = 'passwordtest';
        scope.login(scope.user);
        expect(scope.user.username).toEqual('test');
        expect(scope.user.password).toEqual('passwordtest');
    }));
});
