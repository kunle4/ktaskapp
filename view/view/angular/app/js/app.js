'use strict';


// Declare app level module which depends on filters, and services
angular.module('ktaskApp', ['ktaskApp.services','ktaskApp.controllers','ngResource','kTaskApp.directives','ui.state']).
  config(['$routeProvider','$stateProvider','$urlRouterProvider', function($routeProvider,$stateProvider,$urlRouterProvider) {
    var templateRootPath = 'view/angular/app/partials/';

    $routeProvider.when('/', {redirectTo:'/projects'})
    .when('/projects/:id/tasks', {templateUrl: templateRootPath +'tasks.html', controller: 'tasksCtrl'})
    .when('/projects', {templateUrl: templateRootPath +'main.html', controller: 'projectCtrl'})
    .otherwise({redirectTo: '/'});

    /*$stateProvider
        .state('projects', {
        url: '/projects',
        abstract: true,
        templateUrl: templateRootPath +'main.html',
        controller: 'projectCtrl'
        }); */
  }]);
