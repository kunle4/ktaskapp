'use strict';

/* Directives */


angular.module('kTaskApp.directives', [])
    .directive('ngTemplate', function($http, $compile, templateRootPath) {
        return {
            restrict: 'A',
            transclude: true,
            replace: true,
            link: function(scope, element, attrs)
            {
                var templatePath = attrs.ngTemplate;
                if(templateRootPath == undefined) {
                    throw 'templateRootPath "value" is not defined'
                }

                if(templatePath == undefined || templatePath.length < 1){
                    console.error('Template name needed!');
                    return;
                }
                templatePath =  templateRootPath + templatePath + '.html';

                $http.get(templatePath).
                    success(
                    function(data, status, headers, config) {
                        $compile(element.html(data).contents())(scope.$new());
                    }).
                    error(function(data, status, headers, config) {
                        console.log(data);
                    });
            }
        };
    })
    .directive('teaser', function($compile) {
        return {
            restrict: 'A',
            scope:{
                limit: "&",
                teaser: "&"
            },
            link: function(scope, element, attrs)
            {
                var teaserFunc = function(text, limit) {
                    if(text.length > limit)
                        return text.substring(0,limit) + ' ...';
                    return text;
                }
                element.html(teaserFunc(scope.teaser(), scope.limit()));
            }
        };
    })
    .directive('numCap', function($compile) {
        return {
            restrict: 'A',
            scope:{
                limit: "&",
                numCap: "&"
            },
            link: function(scope, element, attrs)
            {
                var numCapFunc = function(num, limit) {
                    if(num > limit)
                        return limit +"+";
                    return num;
                };
                element.html(numCapFunc(scope.numCap(), scope.limit()));
            }
        };
    });
