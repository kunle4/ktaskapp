'use strict'

/* Services */
angular.module('ktaskApp.services', [])
    .factory('resource', ['$resource',function($resource) {
        var obj = {};
        obj.getProjectResource = function () {
            return $resource('/projects/:id');
        };
        obj.getTasksPerProjectResource = function () {
            return $resource('/projects/count_per_project');
        };
        obj.getCurrentUserResource = function () {
            return $resource('/user/current');
        };
        obj.getTaskResource = function () {
            return $resource('/projects/:id/tasks/');
        };
        return obj;
    }])
    .factory('projectUtils', [function(){
        var obj = {};
        obj.loadProjectStats = function(projects){
            _.each(projects, function(project){
                project.stats = {};
                project.stats.tasksCount = project.tasks.length;
                project.stats.tasksCompleted = _.filter(project.tasks, function(task){ return task.complete;}).length;
                project.stats.percentCompleted = project.stats.tasksCount== 0 ? 0 : ((project.stats.tasksCompleted/project.stats.tasksCount) * 100).toFixed(0);
            });
        }
        return obj;
    }])
    .value('templateRootPath', '/view/angular/app/partials/');


