'use strict';

/* Controllers */

angular.module('ktaskApp.controllers', []).
    controller('projectCtrl', ['$scope','resource','projectUtils',function($scope, resourceService,projectUtils) {
        var Project = resourceService.getProjectResource();
        var TasksPerProject = resourceService.getTasksPerProjectResource();

        $scope.projects = [];
        $scope.projects = Project.query(function(){
            projectUtils.loadProjectStats($scope.projects);
        });
        $scope.tasksPerProject = [];
        $scope.tasksPerProject = TasksPerProject.query();

    }])
    .controller('userCtrl', ['$scope','resource',function($scope,resourceService) {
        var CurrentUser = resourceService.getCurrentUserResource();
        $scope.user = {};
        $scope.user = CurrentUser.get();
    }])
    .controller('tasksCtrl', ['$scope','resource','$routeParams',function($scope,resourceService, $routeParams) {
        alert('we got to the tasksCtrl' +$routeParams.id);
        /*var Task = resourceService.getTaskResource();
        $scope.tasks = [];
        $scope.tasks = Task.query();*/
    }]);