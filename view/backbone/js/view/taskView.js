window.TaskView = Backbone.View.extend({
	initialize : function(options){
		this.template = options.template;//$('#task-template');
		_.bindAll(this, "render");
	},
 	tagName:"div",
	render : function(){
		var htemplate = Handlebars.compile(this.template.html());
		this.el = htemplate(this.model.toJSON());
		return this;
	},
});

window.TaskNewView = Backbone.View.extend({
	initialize : function(){
		_.bindAll(this, ["render"]);
	},
	el: "#task-template-edit",
	render: function(){
		console.log('in render new');
		var htemplate = Handlebars.compile(this.$el.html());
		this.el = htemplate(this.model.toJSON());
		$('#temp-container').html(this.el);
		$('#div-edit-').modal({});	
		//$('#add-subtask').on('click',this.addSubTask);
		$('.tags-input').tagsInput(
			{
				'defaultText':'Tag this task',
				'width':'97%',
				'placeholderColor' : '#CCC'

			}
		);
		$( "#task-group" ).autocomplete({
      		source: taskManager.taskGroupArray
	   	});
		$('#new-todo').on('keypress', this, this.addSubTaskOnEnter);
		$('#save-').on('click',this.save);
		$("#subtask-container").sortable();
		
	},
	save : function(){
		console.log('going to add new task');
		var newTask = {};
		newTask.title = $('#task-title').val();
		newTask.description = $('#task-description').val();
		newTask.group = $('#task-group').val();
		var subtasks = [];
		$('#subtask-container').find('input.subtask-text').each(function(){
			subtasks.push({todo:$(this).val()});
		});
		newTask.tasks = subtasks;

		var tags = $("[data-tags-id]").val().split(',');
		if(tags != null && tags.length > 0 && tags[0].length > 0)
			newTask.tags = tags;

		if(typeof taskManager.currentProject !== "undefined" && taskManager.currentProject != null)
			newTask.project = taskManager.currentProject;

		var task  = new Task(newTask);
		attributes = task.toJSON();
		task.save(null, 
			{
				success: function(data, response){		
					console.log('back from the server after saving');
					//console.log(response);
					$('#div-edit-').modal('hide');
					var task = new Task(response.task);
					taskCollection.add(task); //update the collection
					return task;
				},
				error: function(){console.log('error');}
			}
		);
	},
	addSubTaskOnEnter: function( event ) {
		console.log("in the addSubTaskOnEnter function");

		if ( event.which !== 13 || !$(this).val().trim() ) {
			return;
		}
		var $element = $(this);
		var newView = new SubTaskView({model: new SubTask({todo:$element.val()})});

		$('#subtask-container').prepend(newView.render().el);
		$element.val('');				
	}
});

window.TaskCollectionView = Backbone.View.extend({
	initialize : function(options){
		_.bindAll(this, "render", "renderOne", "removeOne", "delete");
		this.model.on("add", this.renderOne, this);
		//this.model.on("remove", this.removeOne, this);
		this.editMode = false;
	},
	el: "#items-container",
	events : {
		"dblclick [data-task-id]" : "renderEdit",		
		"click .delete-task": "delete",
	},
	render : function(){
		$("#items-container").html('');
		for (var i = this.model.models.length - 1; i >= 0; i--) {
			var task = this.model.models[i];
			this.$el.append(new TaskView({model:task, template: $(this.options.templateSelector)}).render().el);
		}
		taskManager.updateStats();
		return this;
	},
	renderOne : function(item){
		this.$el.append(new TaskView({model: item,  template: $(this.options.templateSelector)}).render().el);
		this.scrollToItem('#div-'+item.id);
		taskManager.updateStats();
	},
	removeOne : function(item){
		console.log('in remove view going to remove');
		//console.log(item);
		this.$el.children().remove('#'+item.id);
		if(this.isEditMode()){
			$('#div-edit-'+item.id).modal('hide');
			$('#temp-container').html('');
		};
		taskManager.updateStats();
		return this;
	},
	delete : function(event){
		console.log('going to remove this task');
		//console.log(this);
		var taskId;
		var task;

		for(i in event.currentTarget.attributes){
			if(event.currentTarget.attributes[i].isId){
				taskId = event.currentTarget.attributes[i].value.replace("delete-","");
				break;
			}
		}

		console.log('The id to delete = '+taskId);
		task = this.model.get(taskId);
        task.set({project:taskManager.currentProject});
        task.destroy(
			{
				success: function(task, response){		
					console.log('back from the server after deleting');	
					taskCollection.remove(task.id);
					taskManager.taskView.removeOne(task);	
					taskManager.taskView.reproritize();	
				},
				error: function(error){console.log(error);}
			},{wait:true});
	},
	reproritize : function(){
		var tasks = taskCollection.models;
		var priorities = [];
		var count = 0;
		console.log('reproritize');
		//console.log(this);
		for (var i = tasks.length - 1; i >= 0; i--) {
			priorities[count] = tasks[i].get('_id');
			count++;
		}
		taskManager.updateTasksPriority(priorities);
	},
	scrollToItem : function(selector){
		$('html, body').animate({
			scrollTop: $(selector).offset().top	
		}, 500);
	},
	renderEdit: function(event){
		this.options.renderEditMode.renderEdit(event, this.options.renderEditMode.templateSelector);	
	},
	isEditMode : function(){
		return this.editMode;
	},
	renderNew : function(){
		var newView = new TaskNewView({model: new Task(), taskCollection: this.model});
		newView.render();
	},
	save : function(event){
		console.log('going to save task');
		var task = event.data.model.get($('[id^="div-edit"]').attr('id').replace('div-edit-',''));
		
		task.set({title:$('#task-title').val()});
		task.set({description:$('#task-description').val()});
		task.set({group:$('#task-group').val()});
		var subtasks = [];
		$('#subtask-container').find('.subtask-text').each(function(){
			var isComplete = false;
			var subtaskId = this.id.replace('subtask-','');
			isComplete = $(this).prev('input').is(':checked');
			if(subtaskId != '')
				subtasks.push({todo:$(this).val(),complete : isComplete, _id: subtaskId});
			else
				subtasks.push({todo:$(this).val()});
		});
		task.set({tasks:subtasks});	
		//console.log("The tags:", $("[data-tags-id="+task.id+"]").val().split(','));
		var tags = $("[data-tags-id="+task.id+"]").val().split(',');
		if(tags != null && tags.length > 0 && tags[0].length > 0)
			task.set({tags: tags});
		if(typeof taskManager.currentProject !== "undefined" && taskManager.currentProject != null)
			task.set({project:taskManager.currentProject});	
		$('[id^="div-edit-"]').modal('hide');
		task.save(null, 
			{
				success: function(data, response){		
					console.log('back from the server after saving');			
					var thisCollection = data.collection;
					thisCollection.fetch({
						success: taskManager.onSuccessTasksFetch,
						error: taskManager.onErrorTasksFetch	
					});
					taskManager.updateStats();
				},
				error: function(){console.log('error');},
				
			});	
	},
	addSubTaskOnEnter: function( event ) {
		console.log("in the addSubTaskOnEnter function");

		if ( event.which !== 13 || !$(this).val().trim() ) {
			return;
		}
		var $element = $(this);
		var newView = new SubTaskView({model: new SubTask({todo:$element.val()})});

		$('#subtask-container').prepend(newView.render().el);
		$element.val('');				
	},
	prioritizeSubtask : function(){
		var subtaskId = $(this).attr('id').replace('subtask-status-','');
		var subtaskToMove = $('#subtask-'+subtaskId);
		var subtaskContainers = $('#subtask-container').children('.subtask-edit');
		var placeBeforeContainer;
		var placeAfterContainer;

		if(this.checked){
			//loop though container and move checked to top of checked			
			for (var i = 0; i < subtaskContainers.length; i++) {				
				if($(subtaskContainers[i]).children('[type="checkbox"]').is(':checked')){
					if($(subtaskContainers[i]).attr('id') != subtaskToMove.attr('id')){
						placeBeforeContainer = $(subtaskContainers[i]);
						break;
					}
				}
				if(placeBeforeContainer == null && typeof placeBeforeContainer === "undefined"){
					if(subtaskToMove.attr('id') != $(subtaskContainers[subtaskContainers.length - 1]).attr('id'))
						placeAfterContainer = $(subtaskContainers[subtaskContainers.length - 1]);
					else
						return;
				}
			}
		}else{
			//move unchecked to top
			if(subtaskToMove.attr('id') != $(subtaskContainers[0]).attr('id') )
				placeBeforeContainer = $(subtaskContainers[0]);	
			else
				return;				
		}

		if(placeBeforeContainer != null && typeof placeBeforeContainer !== "undefined"){
			subtaskToMove.fadeOut(500, function(){
	        	subtaskToMove.insertBefore(placeBeforeContainer);
	        	subtaskToMove.fadeIn(500);
	        });
		}
		else{
			subtaskToMove.fadeOut(500, function(){
	        	subtaskToMove.insertAfter(placeAfterContainer);
	        	subtaskToMove.fadeIn(500);
	        });
		}
	}
});

window.UserView = Backbone.View.extend({
	initialize : function(){
		this.template = $('#header-user-info-template');
	},
 	el: "#header-user-info",
	render : function(){
		var htemplate = Handlebars.compile(this.template.html());
		this.$el.prepend(htemplate(this.model.toJSON()));
		return this;
	}
});

window.UserRegView = Backbone.View.extend({
	initialize : function(){
		this.template = $('#user-registration-template');
	},
	events : {
		'click #login-link' : "renderLogin",
		'click #register-button' : "validate"
	},
	el: "#container",
	render : function(){
		var htemplate = Handlebars.compile(this.template.html());
		this.$el.html(htemplate({}));
		return this;
	},
	renderLogin : function(){
		var loginView = new UserLoginView();
		loginView.render();
		loginView = null;
		return false;
	},
	confirmPassword : function (){
		if($.trim($('#password').val()).length == 0 || $.trim($('#password2').val()).length ==0){
			alert('Bad password confirmation!');
			return false;
		}
		if($('#password2').val() != $('#password').val()){
			alert('Bad password confirmation!');
			return false;
		}

		return true;
	},
	validate : function (){
	if(this.confirmPassword())
		$('#registration-form').submit();
	}
});

window.UserLoginView = Backbone.View.extend({
	initialize : function(){
		this.template = $('#user-login-template');
	},
	events : {
		"click #register-link":"renderRegister"
	},
	el: "#container",
	render : function(){
		var htemplate = Handlebars.compile(this.template.html());
		this.$el.html(htemplate({}));
		new MessagesView({el:'#login-form'}).render();
		return this;
	},
	renderRegister : function(){
		var regView = new UserRegView();
		regView.render();
		regView = null;
		return false;
	}
});

window.MessagesView = Backbone.View.extend({
	initialize : function(){
		this.template = $('#messages-template');
	},
	el: "#container",
	render : function(){	
		var self = this;	
		$.getJSON('/login/messages', function(data) { 
	    	if(data){
		    	var htemplate = Handlebars.compile(self.template.html());
		    	//console.log(data);
		    	self.$el.prepend(htemplate({messages :data, type: 'alert-error'}));
		    	$.post("/messages/clear", function(data){
		    		
	    		});
	    	}
		});	
		
		return this;
	}
});

window.SubTaskView = Backbone.View.extend({
	initialize : function(){
		this.template = $('#subtask-template');
		_.bindAll(this, "render");
	},
 	tagName:"div",
	render : function(){
		console.log('in the render sub taskView')
		var htemplate = Handlebars.compile(this.template.html());
		this.el = htemplate(this.model.toJSON());
		return this;
	}
});

window.SubTaskCollectionView = Backbone.View.extend({
	initialize : function(collection){
		_.bindAll(this, "render");
	},
	el: "#subtask-container",

	render : function(){
		for (var i = this.model.models.length - 1; i >= 0; i--) {
			var subTask = this.model.models[i];
			this.$el.append(new SubTaskView({model:subTask}).render().el);
		}
		return this;
	},
	removeOne : function(item){
		console.log('in remove subtask');
		return this;
	},
	renderNew : function(){
		//var newView = new SubTaskView({model: new Task(), subTaskCollection: this.model});
		//newView.render();
	}
});

//status
window.StatusView = Backbone.View.extend({
	initialize : function(){
		this.template = $('#status-template');
		_.bindAll(this, "render");
	},
 	tagName:"ul",
	render : function(){
		//console.log('Building each status item');
		var htemplate = Handlebars.compile(this.template.html());
		this.el = htemplate(this.model.toJSON());
		return this;
	},
});

window.StatusCollectionView = Backbone.View.extend({
	initialize : function(){
		_.bindAll(this, "render");
	},
 	el: "#status-container",
	render : function(){
		//console.log('Building status sidebar');
		//console.log(this.model.models);
		this.$el.html('');
		for (var i = this.model.models.length - 1; i >= 0; i--) {
			var status = this.model.models[i];
			this.$el.append(new StatusView({model:status}).render().el);
		}
		return this;
	}
});