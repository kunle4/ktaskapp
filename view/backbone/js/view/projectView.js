window.ProjectView = Backbone.View.extend({
	initialize : function(){
		this.template = $('#project-item-template');
		_.bindAll(this, "render");
	},
	tagName: "li",
	render : function(){
		var htemplate = Handlebars.compile(this.template.html());
		this.el = htemplate(this.model.toJSON());
		this.delegateEvents();
		return this;
	}
});

window.ProjectNewView = Backbone.View.extend({
	initialize : function(){
		_.bindAll(this, ["render"]);
	},
	el: "#project-template-edit",
	
	render: function(){
		console.log('in render new project');
		var htemplate = Handlebars.compile(this.$el.html());
		this.el = htemplate(this.model.toJSON());
		$('#temp-container').html(this.el);
		$('#div-edit-').modal({});	$('#div-edit-').modal("show");
		$('#save-project-').on('click',this.save);		
	},
	save : function(){
		console.log('going to add new project');
		var project  = new Project({title: $('#project-title').val(), description: $('#project-description').val()});
		//attributes = project.toJSON();
		project.save(null, 
			{
				success: function(data, response){		
					console.log('back from the server after saving');
					//console.log(response);
					$('#div-edit-').modal('hide');
					var project = new Project(response);							
					taskManager.projectCollection.add(project); //update the collection
					return project;
				},
				error: function(){console.log('error');}
			}
		);
	}
});

window.ProjectCollectionView = Backbone.View.extend({
	initialize : function(collection){
		_.bindAll(this, "render", "renderOne", "removeOne");
		this.model.on("add", this.renderOne, this);
		//this.model.on("remove", this.removeOne, this);
		this.editMode = false;
		this.editingTask = null;
	},
	el: "#items-container",
	events : {
		"click .view-tasks-link" : "switchToTasks",
		"click .add-task-link" : "addTask",
		"dblclick .project-tile" : "renderEdit"
	},
	render : function(){
		$("#items-container").html('');
		for (var i = this.model.models.length - 1; i >= 0; i--) {
			var project = this.model.models[i];
            var stats = {};
            stats.tasksCount = project.attributes.tasks.length;
            stats.tasksCompleted = _.filter(project.attributes.tasks, function(task){ return task.complete;}).length;
            stats.percentCompleted = stats.tasksCount== 0 ? 0 : ((stats.tasksCompleted/stats.tasksCount) * 100).toFixed(0);
            project.set({stats: stats});
			this.$el.append(new ProjectView({model:project}).render().el);
		}
		
		return this;
	},

	renderOne : function(item){
		console.log(item);
		this.$el.append(new ProjectView({model:item}).render().el);
		this.scrollToItem('#div-'+item.id);
	},
	removeOne : function(item){
		console.log('in remove view going to remove');
		//console.log(item);
		this.$el.children().remove('#'+item.id);
		if(this.isEditMode()){
			$('#div-edit-'+item.id).modal('hide');
			$('#temp-container').html('');
		};
		return this;
	},
	scrollToItem : function(selector){
		$('html, body').animate({
			scrollTop: $(selector).offset().top	
		}, 500);
	},
	renderNew : function(){
		var newView = new ProjectNewView({model: new Project(), projectCollection: this.model});
		newView.render();
	},
	renderEdit: function(event){
		console.log('in render edit');
		console.log($(event.currentTarget).attr("data-project-id"));
		var projectId = $(event.currentTarget).attr("data-project-id");
		var project = taskManager.projectView.model.get(projectId);
		
		var htemplate = Handlebars.compile($('#project-template-edit').html());
		var editEl = htemplate(project.toJSON());
		$('#temp-container').html(editEl);
		$('#div-edit-'+project.id).modal({});
		
		$('[id^="save-"]').on('click',taskManager.projectView, taskManager.projectView.save);	
	},
	save : function(event){
		console.log('going to save project');
		var project = event.data.model.get($('[id^="div-edit"]').attr('id').replace('div-edit-',''));
		
		project.set({title:$('#project-title').val()});
		project.set({description:$('#project-description').val()});
		$('[id^="div-edit-"]').modal('hide');
		project.save(null, 
			{
				success: function(data, response){		
					console.log('back from the server after saving project');			
					var thisCollection = data.collection;
					thisCollection.fetch({
						success: function(){taskManager.projectView.render();},
						error: function(){console.log('error');}	
					});
					taskManager.updateStats();
				},
				error: function(){console.log('error');},
				
			});	
	},
	switchToTasks : function(event){
		console.log("clicked swtich to tasks view");
		var projectId = $(event.currentTarget.outerHTML).attr('data-project-id');
		var options = {status: "ongoing", project: projectId};
		taskManager.currentProject = projectId;
		taskManager.loadTasks(options, taskManager.makeTaskContainerSortable);
		return false;
	},
	addTask : function(event){
		console.log("clicked add task on project dashboard");
		var projectId = $(event.currentTarget.outerHTML).attr('data-project-id');
		var options = {status: "ongoing", project: projectId};
		taskManager.currentProject = projectId;
		var callback = function(){
			taskManager.makeTaskContainerSortable();
			taskManager.taskView.renderNew();
		};
		taskManager.loadTasks(options, callback);
		return false;
	}
	
});